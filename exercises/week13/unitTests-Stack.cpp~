#include "catch.hpp" // simple unit-testing framework
#include "Stack.hpp" // the functions we want to test

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::vector;

/* define output operator for std::vector so we can test a Stack<vector<T>>
 * (since we print our stacks, and the output operator for the stack relies
 * on there being an output operator for the stack "payload" type)
 */
template <typename T>
std::ostream & operator<< (std::ostream &os, const vector<T> &v) {
  os << "[ ";
  for (const auto &i : v) {
    os << i << " ";
  }
  os << "]";
  return os;
}

/*******************************
 * Unit tests for the Stack class and its member functions.
 *
 * Note that all unit tests rely on the output operator
 * working correctly; this means that if operator<< doesn't
 * work, then probably none of the tests will pass even if your
 * other code is right.  It would be nice if we could test
 * the output operator by itself, but since there's no way
 * to get data into a stack other than the push method,
 * it's not clear how we'd test the output operator without
 * also testing push at the same time without giving the
 * testing code access to the private class data members
 * (which wouldn't necessarily be bad, but it would mean
 * we'd need to write a member function that ran the
 * tests for us).
 *******************************/



/* basic tests for the push method; rely on operator<< */
TEST_CASE("push<int>", "[push, int]") {

  Stack<int> s;
  s.push(1);
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "1\n");

  s.push(2);
  s.push(3);
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "3\n2\n1\n");
  
}

/* same tests, but using a double as the payload */ 
TEST_CASE("push<double>", "[push, double]") {

  Stack<double> s;
  s.push(1.1);
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "1.1\n");

  s.push(2.99);
  s.push(3.1415);
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "3.1415\n2.99\n1.1\n");
  
}

/* same tests, this time with strings */ 
TEST_CASE("push<string>", "[push, string]") {

  Stack<string> s;
  s.push("foo");
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "foo\n");

  s.push("bar");
  s.push("baz");
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "baz\nbar\nfoo\n");
  
}

/* same tests, this time with vectors of integers */ 
TEST_CASE("push<vector<int>>", "[push, int, vector]") {

  Stack<vector<int>> s;
  s.push({1});
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "[ 1 ]\n");

  s.push({2, 3, 4});
  s.push({5, 6, 7, 8});
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "[ 5 6 7 8 ]\n[ 2 3 4 ]\n[ 1 ]\n");
  
}



/* tests for the top method; rely on push */
TEST_CASE("top<int>", "[top, int]") {
  Stack<int> s;
  //attempt top on empty stack
  REQUIRE_THROWS_AS(s.top(), std::range_error);

  s.push(1);
  CHECK(s.top() == 1);
  s.push(2);
  CHECK(s.top() == 2);  
  CHECK(s.top() == 2);  //repeat to be sure that top doesn't pop
}


/* tests for the pop method; rely on push */
TEST_CASE("pop<int>", "[pop, int]") {
  Stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);
  CHECK(s.pop() == 3); //pop the 3
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "2\n1\n");
  
  CHECK(s.pop() == 2); //pop the 2
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "1\n");

  CHECK(s.pop() == 1);  //pop the 1
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "");

  //try to pop on empty stack
  REQUIRE_THROWS_AS(s.pop(), std::underflow_error);

}


/* more advanced tests for the push method to check resizing; rely on operator<< */
TEST_CASE("push beyond original capacity<int>", "[push, int]") {

  Stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);

  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "3\n2\n1\n");

  s.push(4);
  s.push(5);
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "5\n4\n3\n2\n1\n");

  s.push(6);
  s.push(7);
  s.push(8);
  s.push(9);
  
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "9\n8\n7\n6\n5\n4\n3\n2\n1\n");
  
}

/* tests for the size method; rely on push, pop, top */
TEST_CASE("size<double>", "[size, double]") {
  Stack<double> s;
  CHECK(s.size() == 0);
  s.push(1);
  s.push(2);
  CHECK(s.size() == 2);
  s.push(3.4);
  s.push(4.73245);
  s.push(5.2);
  CHECK(s.size() == 5);
  CHECK(s.pop() == 5.2);
  CHECK(s.pop() == 4.73245);
  CHECK(s.size() == 3);
  CHECK(s.top() == 3.4);  //top, not pop!
  CHECK(s.top() == 3.4);  //top, not pop!
  CHECK(s.size() == 3);
}

/* tests for the empty method; rely on push, pop, top */
TEST_CASE("empty<int>", "[empty, int]") {
  Stack<int> s;
  CHECK(s.empty());
  s.push(1);
  s.push(2);
  CHECK(!s.empty());
  CHECK(s.top() == 2);  //top, not pop!
  CHECK(s.top() == 2);  //top, not pop!
  CHECK(!s.empty());
  CHECK(s.pop() == 2);  //remove 2
  CHECK(!s.empty());
  CHECK(s.top() == 1);  //top, not pop!
  CHECK(!s.empty());
  CHECK(s.pop() == 1);  //remove 1
  CHECK(s.empty());
}

/* tests for the clear method; rely on push */
TEST_CASE("clear<int>", "[clear, int]") {
  Stack<int> s;
  s.clear();  //clear an empty stack
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "");

  s.push(1);
  s.push(2);
  s.push(3);
  s.clear(); //clear a non-empty stack
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "");

}


/* tests for the swap method; rely on push */
TEST_CASE("swap<int>", "[swap, int]") {

  Stack<int> s;
  //attempt to swap on stack with no elements
  REQUIRE_THROWS_AS(s.swap(), std::range_error);

  s.push(1);
  //attempt to swap on stack with only one element
  REQUIRE_THROWS_AS(s.swap(), std::range_error);
      
  s.push(2);
  s.push(3);
  
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "3\n2\n1\n");

  s.swap();  //swap with default offset of one
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "2\n3\n1\n");

  s.swap(2);
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "1\n3\n2\n");

}



// tests for the copy constructor; rely on push, pop, size
TEST_CASE("copy constructor<int>", "[copy constructor, int]") {
  Stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);
  s.push(4);
  s.push(5);
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "5\n4\n3\n2\n1\n");
  CHECK(s.size() == 5);

  Stack<int> t = s;  //now t should look the same as s
  std::stringstream stream2;
  stream2 << t;
  CHECK(stream2.str() == "5\n4\n3\n2\n1\n");
  CHECK(t.size() == 5);

  //let's change s a bit
  CHECK(s.pop() == 5);
  CHECK(s.pop() == 4);
  s.push(6);
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "6\n3\n2\n1\n");
  CHECK(s.size() == 4);

  //let's ensure that changing contents of s didn't change t
  std::stringstream stream4;
  stream4 << t;
  CHECK(stream4.str() == "5\n4\n3\n2\n1\n");
  CHECK(t.size() == 5);

  //now let's change t
  CHECK(t.pop() == 5);
  t.push(7);
  t.push(8);
  std::stringstream stream5;
  stream5 << t;
  CHECK(stream5.str() == "8\n7\n4\n3\n2\n1\n");
  CHECK(t.size() == 6);
  
  //let's ensure that changing contents of t didn't change s
  std::stringstream stream6;
  stream6 << s;
  CHECK(stream6.str() == "6\n3\n2\n1\n");
  CHECK(s.size() == 4);

}

/* same tests, but using Stack<vector<string>> */ 
TEST_CASE("copy constructor<vector<string>>", "[copy constructor, vector, string]") {
  Stack<vector<string>> s;
  s.push({"Mary"});
  s.push({"had", "a"});
  s.push({"little", "lamb", "its", "fleece"});
  s.push({"as", "white", "as"});
  s.push({"snow"});
  std::stringstream stream;
  stream << s;
  //CHECK(stream.str() == "5\n4\n3\n2\n1\n");
  CHECK(stream.str() == "[ snow ]\n[ as white as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(s.size() == 5);

  Stack<vector<string>> t = s;  //now t should look the same as s
  std::stringstream stream2;
  stream2 << t;
  //CHECK(stream2.str() == "5\n4\n3\n2\n1\n");
  CHECK(stream2.str() == "[ snow ]\n[ as white as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(t.size() == 5);

  //let's change s a bit
  CHECK(s.pop() == vector<string>({"snow"})); // NOTE: need slightly awkward syntax for the vector because CHECK is a macro, which means type inference isn't as good as we'd like
  CHECK(s.pop() == vector<string>({"as", "white", "as"}));
  s.push({"as", "black", "as"});
  std::stringstream stream3;
  stream3 << s;
  //CHECK(stream3.str() == "6\n3\n2\n1\n");
  CHECK(stream3.str() == "[ as black as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(s.size() == 4);

  //let's ensure that changing contents of s didn't change t
  std::stringstream stream4;
  stream4 << t;
  //CHECK(stream4.str() == "5\n4\n3\n2\n1\n");
  CHECK(stream4.str() == "[ snow ]\n[ as white as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(t.size() == 5);

  //now let's change t
  CHECK(t.pop() == vector<string>({"snow"}));
  t.push({"spring"});
  t.push({"green", "grass"});
  std::stringstream stream5;
  stream5 << t;
  CHECK(stream5.str() == "[ green grass ]\n[ spring ]\n[ as white as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(t.size() == 6);
  
  //let's ensure that changing contents of t didn't change s
  std::stringstream stream6;
  stream6 << s;
  CHECK(stream6.str() == "[ as black as ]\n[ little lamb its fleece ]\n[ had a ]\n[ Mary ]\n");
  CHECK(s.size() == 4);

}


// tests for the assignment operator; rely on push, pop, size 
// These are same general tests as for copy constructor, 
// we just changed how copy t gets created 
TEST_CASE("operator=<int>", "[operator=, int]") {
  Stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);
  s.push(4);
  s.push(5);
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "5\n4\n3\n2\n1\n");
  CHECK(s.size() == 5);

  Stack<int> t; //declare first, then assign on next line
  t = s;  //now t should look the same as s
  std::stringstream stream2;
  stream2 << t;
  CHECK(stream2.str() == "5\n4\n3\n2\n1\n");
  CHECK(t.size() == 5);

  //let's change s a bit
  CHECK(s.pop() == 5);
  CHECK(s.pop() == 4);
  s.push(6);
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "6\n3\n2\n1\n");
  CHECK(s.size() == 4);

  //let's ensure that changing contents of s didn't change t
  std::stringstream stream4;
  stream4 << t;
  CHECK(stream4.str() == "5\n4\n3\n2\n1\n");
  CHECK(t.size() == 5);

  //now let's change t
  CHECK(t.pop() == 5);
  t.push(7);
  t.push(8);
  std::stringstream stream5;
  stream5 << t;
  CHECK(stream5.str() == "8\n7\n4\n3\n2\n1\n");
  CHECK(t.size() == 6);
  
  //let's ensure that changing contents of t didn't change s
  std::stringstream stream6;
  stream6 << s;
  CHECK(stream6.str() == "6\n3\n2\n1\n");
  CHECK(s.size() == 4);

}

