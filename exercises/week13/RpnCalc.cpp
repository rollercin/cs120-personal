
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cassert>
#include "Stack.hpp"

using std::cout;
using std::cin;

/* exception class for divide by zero errors; only real
 * difference from parent is the default what() message,
 * but having our own class lets us specifically catch
 * just this error */
class divide_by_zero_error : public std::runtime_error {
  public:
    divide_by_zero_error (const std::string& msg ="attempt to divide by zero") 
    : std::runtime_error(msg) { }
};

/* print the "help" message */
void printHelp() {
  cout << "This is an RPN-style stack calculator.  Simply enter numbers and operators to perform arithmetic.  The letter 'p' will print the value on top of the stack, 'f' will print the entire stack, 's' will swap the top two stack entries, 'c' will clear the stack, 'h' will re-print this message, and 'q' will quit.\n";
}

//using number_t = int;
using number_t = double;

/* main program */
int main() {
  Stack<number_t> stack;
  number_t num;
  std::string op;

  // start by printing help message
  printHelp();

  for(;;) { // processing loop, run until program is over
    cin >> op; // try to read next term from input
    if (cin.eof() || cin.fail()) { // if we can't, not much else we can do but quit
      std::cerr << "cannot read input; exiting.\n";
      return -1;
    }

    // see if it's a number
    std::stringstream ss(op);
    ss >> num; // will only work if the term can be read into num's type
    if (!ss.fail()) { // check if it worked or not
      stack.push(num); // push new number onto stack
    } else { // input wasn't a number, so it will be treated as an op/command

      if ( op == "q" || op == "quit" || op == "exit") { // user asked to quit
        // no need to say anything special, just break out of the processing loop
        break;
      }

      // try to apply the operator or command 
      try {
        if (op == "p") { // print top
          cout << stack.top() << "\n";
        } else if (op == "f") { // print whole stack
          cout << stack;
        } else if (op == "s") { // swap
          stack.swap();
        } else if (op == "c") { // clear
          stack.clear();
        } else if (op == "h") { // help
          printHelp();
        } else if (op == "+" || op == "-" || op == "*" || op == "/") { // binary operators
          number_t right = stack.pop();
          number_t left = stack.pop();
          number_t result;
          if (op == "+") { // addition
            result = left + right;
          } else if (op == "-") { // subtraction
            result = left - right;
          } else if (op == "*") { // multiplication
            result = left * right;
          } else if (op == "/") { // division
            if (right == 0) {
              throw divide_by_zero_error();
            }
            result = left / right;
          } else {
            assert(false); // should never get here
          }
          stack.push(result);
        } else { // unknown command
          cout << "Unknown command: \""<<op<<"\" (type 'h' for help)\n";
        }
      // catch exceptions we know how to handle
      } catch (std::underflow_error &e) {
        cout << "operation failed: stack underflow detected.\n";
      } catch (std::range_error &e) {
        cout << "operation failed: attempt to access invalid stack element.\n";
      } catch (divide_by_zero_error &e) {
        cout << "operation failed: attempt to divide by zero.\n";
      }

    }

  }

  cout << "Goodbye!\n";

  return 0;
}
