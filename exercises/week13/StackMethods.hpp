#ifndef _CS120_STACKMETHOD_HPP
#define _CS120_STACKMETHOD_HPP

#include <stdexcept>
#include <cassert>

/* constructor 
   Creates a new object with default capacity (dataLength)
   equal to 4 and a stackDepth equal to 0.  Allocates the
   size-dataLength int array that data will point to.
*/
template <typename T>
Stack<T> :: Stack() {
  dataLength = 4; //default capcacity of stack is 4 ints
  stackDepth = 0; //stack begins empty
  data = new T[dataLength]; //allocate array to hold ints
}

/* copy constructor 
   Creates a new object based on data in other.  Need to write
   this ourselves because we're using dynamic memory allocation
   and want a "deep copy" of the array holding the stack items.
   (Otherwise, the automatically-created copy constructor would be fine.)
*/

template <typename T>
Stack<T> :: Stack (const Stack<T> &other) {

  //depth >= length shouldn't be possible, but check anyway
  assert(other.stackDepth < other.dataLength); 

  //assign values to this object's instance variables, and
  //allocate memory for the stack before copying over values
  dataLength = other.dataLength;
  stackDepth = other.stackDepth;

  /* Note that we don't call delete[] data; here before allocating;
     this object didn't exist before this moment, so no memory was
     previously allocated for it!   (This is different behavior than 
     in the assignment operator definition.)
  */
  
  data = new T[dataLength];  //allocate same amount of space as other has
  for (size_t i = 0; i < stackDepth; i++) { 
    data[i] = other.data[i];  //copy over data values one by one
  }

}

/* destructor 
   Deallocates anything left in this object that was allocated with "new".
*/
template <typename T>
Stack<T> :: ~Stack () {

  //just deallocate anything we allocated with "new", namely
  //the array pointed to by data
  delete[] data;
}


/* assignment operator
   Copies values from other into this object.  Need write our own operator
   because we use dynamic allocation in this class and want a "deep copy".
*/
template <typename T>
Stack<T> & Stack<T> :: operator= (const Stack<T> &other) {

  if (this == &other) {  //same object; no work to be done
    return *this;
  }

  //copy basic instance variables
  dataLength = other.dataLength;
  stackDepth = other.stackDepth;

  //deallocate the current array, then allocate new one of correct size
  delete[] data;
  data = new T[dataLength];  //same as other.dataLength now

  //copy over in-use elements one at a time
  for (size_t i = 0; i < stackDepth; i++) {
    data[i] = other.data[i];
  }
  
  //return assigned value, which is a reference to this object
  return *this;
}

/* reserve
   Change amount of space allocated for stack element storage.
   Can be use to "reserve" a given amount of space if you know how much you'll
   need (to avoid repeated re-allocating).  This is also used within this class
   to expand storage (by doubling its size) when we run out of space. 
*/
template <typename T>
void Stack<T> :: reserve(size_t len) {
  T * temp = data; //keep track of old data
  data = new T[len]; //allocate new storage

  for (size_t i = 0; (i < len) && (i < dataLength) && (i < stackDepth); i++) {
    data[i] = temp[i]; //copy all elements needed from old to new
  }
  dataLength = len;
  delete[] temp;  //frees old storage
}

/* push
   Adds (a copy of) the new element to the top of the stack.  If array
   of stack items becomes completely full after item is added, this
   method calls reserve to double the size of the existing array
*/
template <typename T>
void Stack<T> :: push (const T & i) {
  assert(stackDepth < dataLength); //depth >= length shouldn't be possible, but check anyway
  data[stackDepth] = i; //copy new element into array
  stackDepth++; //update element count
  if (stackDepth == dataLength) { //we just used up last empty slot
    reserve(dataLength * 2); //double the current length and copy over existing data
  }
}

/* pop
   Removes the top element from the stack, and returns its value.
   Throws std::underflow_error if stack is empty when called.
*/
template <typename T>
T Stack<T> :: pop () {
  //if we try to pop from empty stack, throw an appropriate exception
  if (stackDepth < 1) {
    throw std::underflow_error("attempt to pop from empty stack");
  }

  //it's safe, so update the stackDepth counter and return requested value
  stackDepth--;  // decrement count of items (but don't shorten the array)
  return data[stackDepth]; //ok even though we decremented stackDepth above
}

/* clear
   Removes everything from the stack (actually just changes stackDepth).
*/
template <typename T>
void Stack<T> :: clear() {
  stackDepth = 0; //don't deallocate memory in this method!
}

/* empty
   Return whether stack contains any data items or not.
*/
template <typename T>
bool Stack<T> :: empty() const {
  return size() == 0;
}

/* top
   Returns (a copy of) the top stack element, or throws
   a std::range_error if stack is empty when called.
*/
template <typename T>
T Stack<T> :: top () const {
  //if we try call top on an empty stack, throw an appropriate exception
  if (stackDepth < 1) {
    throw std::range_error("top() called on empty stack");
  }
  //return top stack value
  return data[stackDepth - 1];
}

/* size
   Returns the number of items currently stored in the stack.
   Note that the return value here is unrelated to the capacity of the array.
*/
template <typename T>
size_t Stack<T> :: size() const {
  return stackDepth;
}


/* swap
   Exchanges two elements of the stack.  When called without an argument,
   the default value means it swaps the top two elements.  This version
   also allows other elements to be swapped with the top element by passing
   in argument representing the depth of the item to swap with the top. 
*/
template <typename T>
void Stack<T> :: swap (size_t offset) {
  //don't try to swap elements that don't exist  
  if (offset < 1 || stackDepth < offset+1) {
    throw std::range_error("attempt to swap with insufficient elements");
  }
  //swap specified element values
  T tmp = data[stackDepth-1];
  data[stackDepth-1] = data[stackDepth-offset-1];
  data[stackDepth-offset-1] = tmp;
}


/* output stream operator <<
   Outputs each item from the stack, one per line, into the given stream,
   beginning with the element at the top of the stack.
*/
template <typename U>
std::ostream & operator<< (std::ostream &os, const Stack<U> &s) {

  //"reverse-iterate" over the data array
  for (int i = s.stackDepth - 1; i >= 0; i--) {
    os << s.data[i] << "\n";
  }
  return os;
}

#endif
