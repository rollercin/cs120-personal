#ifndef _CS120_STACK_HPP
#define _CS120_STACK_HPP

#include <iostream>
#include <vector>
#include <iterator>
/* The Stack class should behave like we'd expect from a stack,
 * meaning it's a first-in-last-out datastructure.  Basically,
 * you can only add or remove things at the top of the stack.
 */
class Stack {

public:
  Stack(){} // default constructor (doesn't need to do anything)

  void push(int i); // add a new number to the top of the stack
  int pop(); // remove top number from the stack and return it
  void clear(); // remove all numbers from the stack

  int top() const; // return the value of the top stack element, but don't move it
  bool empty() const; // return true if the stack is empty
  int getsize() const; // return the number of elements in the stack

  void swap(int offset =1);

  const int & operator[](unsigned index) const;
  class iterator;
  iterator begin() const;
  iterator end() const;

protected:

  /* store the numbers in an array for now; we'll switch to doing
   * the memory management ourselves later */
  std::vector<int> data;

public:
  class iterator{
  public:
    iterator(const Stack *p = nullptr, int i = -1) : stack(p), element(i){ }

    int  operator*();
    bool operator != (const iterator &other) const;
    bool operator == (const const_iterator &other) const;
    iterator & operator--();
    
  private:
    const Stack *stack;
    int element;
  };

};
 
 std::ostream & operator<< (std::ostream &os, const Stack &s);
#endif
