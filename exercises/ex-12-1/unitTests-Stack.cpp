#include "catch.hpp" // simple unit-testing framework
#include "Stack.hpp" // the functions we want to test

#include <sstream>

/*******************************
 * Unit tests for the Stack class and its member functions.
 *
 * Note that all unit tests rely on the output operator
 * working correctly; this means that if operator<< doesn't
 * work, then probably none of the tests will pass even if your
 * other code is right.  It would be nice if we could test
 * the output operator by itself, but since there's no way
 * to get data into a stack other than the push method,
 * it's not clear how we'd test the output operator without
 * also testing push at the same time without giving the
 * testing code access to the private class data members
 * (which wouldn't necessarily be bad, but it would mean
 * we'd need to write a member function that ran the
 * tests for us).
 *******************************/


/* tests for the push method; rely on operator<< */
TEST_CASE("push", "[push]") {

  Stack s;
  s.push(1);
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "1\n");

  s.push(2);
  s.push(3);
  
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "3\n2\n1\n");
  
}

/* tests for the pop method; rely on push */
TEST_CASE("pop", "[pop]") {
  Stack s;
  s.push(1);
  s.push(2);
  s.push(3);
  CHECK(s.pop() == 3); //pop the 3
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "2\n1\n");
  
  CHECK(s.pop() == 2); //pop the 2
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "1\n");

  CHECK(s.pop() == 1);  //pop the 1
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "");

  //try to pop on empty stack
  REQUIRE_THROWS_AS(s.pop(), std::underflow_error);

}

/* tests for the clear method; rely on push */
TEST_CASE("clear", "[clear]") {
  Stack s;
  s.clear();  //clear an empty stack
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "");

  s.push(1);
  s.push(2);
  s.push(3);
  s.clear(); //clear a non-empty stack
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "");

}

/* tests for the top method; rely on push */
TEST_CASE("top", "[top]") {
  Stack s;
  //attempt top on empty stack
  REQUIRE_THROWS_AS(s.top(), std::range_error);

  s.push(1);
  CHECK(s.top() == 1);
  s.push(2);
  CHECK(s.top() == 2);  
  CHECK(s.top() == 2);  //repeat to be sure that top doesn't pop
}

/* tests for the swap method; rely on push */
TEST_CASE("swap", "[swap]") {

  Stack s;
  //attempt to swap on stack with no elements
  REQUIRE_THROWS_AS(s.swap(), std::range_error);

  s.push(1);
  //attempt to swap on stack with only one element
  REQUIRE_THROWS_AS(s.swap(), std::range_error);
      
  s.push(2);
  s.push(3);
  
  std::stringstream stream;
  stream << s;
  CHECK(stream.str() == "3\n2\n1\n");

  s.swap();  //swap with default offset of one
  std::stringstream stream2;
  stream2 << s;
  CHECK(stream2.str() == "2\n3\n1\n");

  s.swap(2);
  std::stringstream stream3;
  stream3 << s;
  CHECK(stream3.str() == "1\n3\n2\n");

}

