#include "Stack.hpp"
#include <stdexcept>


/* Adds the argument item to the top of the stack.
 */
void Stack :: push (int i) {
  data.std::vector<int>::push_back(i); 
}


/* Removes most recently-added item from stack, and returns it.
   Throws std::underflow_error if called on an empty stack.
 */
int Stack :: pop () {
  if(data.std::vector<int>::size() == 0){
    throw std::underflow_error("too less element");
  }else{
    int a = data[data.std::vector<int>::size()-1];
    data.std::vector<int>::pop_back();
    return a;
  }
  return -1;
}


/* Empties out the stack.
 */
void Stack :: clear() {
  data.std::vector<int>::clear();
}


/* Returns most recently-added item, but does not remove it.
   Throws std::range_error if called on an empty stack.
 */
int Stack :: top () const {
  if(data.std::vector<int>::size() == 0){
    throw std::range_error("too less element");
  }else{
    int a = data[data.std::vector<int>::size()-1];
    return a;
  }
  return -1;
}

int Stack :: getsize() const {
  return data.size();
}

/* Swaps the item at the top of the stack with the item offset
   positions below it.  Throws std::range_error when the offset
   is too large.
*/
void Stack :: swap (int offset) {
  if(offset >= data.std::vector<int>::size()){
    throw std::range_error("offset to large");
  }
  int temp = data[data.std::vector<int>::size()-1];
  data[data.std::vector<int>::size()-1] = data[data.std::vector<int>::size()-1-offset];
  data[data.std::vector<int>::size()-1-offset] = temp;
}


/* Outputs the contents of the stack, one item per line, with
   the item on the top of the stack appearing first.
 */
/*std::ostream & operator<< (std::ostream &os, const Stack &s) {
  for(std::vector<int>::const_reverse_iterator rit = s.data.rbegin(); rit !=s.data.rend(); ++rit){
    os << *rit << "\n";  
  }
  return os;
  }*/

std::ostream & operator<< (std::ostream &os, const Stack &s){
  for(auto it = s.begin(); it != s.end(); --it){
    os << *it << "\n";
  }
  return os;
}





Stack::iterator Stack::begin() const{
  iterator i(this, getsize()-1);
  return i;
}

Stack::iterator Stack::end() const{
  iterator i(this, -1);
  return i;
}

int Stack::iterator:: operator*() {
  if (stack) {
    return stack->data[element];
  } else {
    throw std::range_error("attempt to dereference invalid iterator");
  }
}

bool Stack::iterator:: operator!=(const iterator &other) const {
  return (stack != other.stack || element != other.element);
}

Stack::iterator & Stack::iterator:: operator--() {
  if (!stack) {
    return *this; // don't mess with null pointers
  }
  --element;
  if (element < 0 ) { // past the end
    element = -1; // mark it as non-dereferenceable
  }
  return *this;
}
