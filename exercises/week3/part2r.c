#include <stdio.h>
#include <string.h>

/* 
   In class exercise Ex-3-.
   Practice using string functions and writing helper methods.
*/



/*
  Returns in the third argument the concatenation of the first
  argument and the second argument, provided that there is
  sufficient space in third argument, as specified by the fourth.
  e.g.
      concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
      concat("alpha", "gamma", result, 10) puts nothing into result and returns 1

 */
int interleave(char word1[],char word2[],char word3[],char result[],int result_cap){
  int x =strlen(word1);
  int y =strlen(word2);
  int z =strlen(word3);
  if(x+y+z+1 >result_cap){
    return 1;

  }
    int big =0;
    int small=0;
    while(small< x || small < y || small< z){
      if(small < x){
	result[big++] = word1[small];

      }
      if(small < y){
        result[big++] = word2[small];
      }
      if(small < z){
        result[big++] = word3[small];

      }
      small++;
    }
    result[big] = '\0';
    return 0;
  }
int concat(char word1[], char word2[], char result[], int result_cap){
  int x = strlen(word1);
  int y = strlen(word2);
  if(x+y+1 >result_cap){
    return 1;

  }else{
    int num =0;
    for(int a=0;a<x;a++){
      result[num]=word1[a];
      num++;
    }
    for(int b=0;b<y;b++){
      result[num]=word2[b];
      num++;

    }
    result[num] = '\0';
    return 0;
  }

}


int main() {

  char word1[11];  //allow up to 10 chars, then room for '/0' terminator
  char word2[11];  //allow up to 10 chars, then room for '/0' terminator
  scanf("%s", word1);
  scanf("%s", word2);
  int result_cap;
  scanf("%d",&result_cap);
  char result[result_cap];
 
  if(!concat(word1, word2, result, result_cap)) {
    printf("Concatenation was successful: %s\n", result);
  } else {
    printf("Concatenation was not successful.\n");
  }

  char word3[11];
  scanf("%s", word3);
  if(!interleave(word1, word2, word3, result, result_cap)){
    printf("Interleave was successful: %s\n", result);
  }else {
    printf("Interleave was not successful.\n");
  }

  return 0;
}

