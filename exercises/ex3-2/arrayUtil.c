#include "arrayUtil.h"
void testPrintArray () {
  int ar1[] = {1, 3, 2, 9, 14};
  double ar2[] = {3.333, 2.4, 7, 9.763, 12.2};

  printArrayInt(ar1, 5);
  printf("\n");
  printArrayDouble(ar2, 5);
  printf("\n");
}

/* function to test getting and then printing an array */
void testGetArray () {
  int ar[5];
  int len = getArrayInt(ar, 5);

  printf("got array: ");
  printArrayInt(ar, len);
  printf("\n");
}

/* pretty-print an array of integers */
void printArrayInt (int array[], int length) {
  printf("[");
  for (int i=0; i<length; ++i) {
    printf(" %d,", array[i]);
  }
  printf("\b ]");
}

/* pretty-print an array of doubles */
void printArrayDouble (double array[], double length) {
  printf("[");
  for (int i=0; i<length; ++i) {
    printf(" %.2f,", array[i]);
  }
  printf("\b ]");
}
int getArrayInt (int array[], int length) {
  printf("lalalalalalal %d integers: ", length);

  int l;
  for (l=0; l<length; ++l) {
    if (!scanf("%d", &array[l])) { // if scanf returns 0, that means it failed
      return l; // give up and say how many things we got
    }
  }

  return l; // l should be the same as length here
}
