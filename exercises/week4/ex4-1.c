
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int main(int argc, char* argv[]){
  FILE *fh;
  FILE *opf;
  float sum =0;
  int num =0;
  int row = atoi(argv[2]);
  int col = atoi(argv[3]);
  fh  = fopen(argv[1], "r");
  opf = fopen(argv[4], "w");
  float arr[row][col];

  for(int a=0; a< row; a++){
    for(int b=0; b<col;b++){
      fscanf(fh,"%f", &arr[a][b]);
      printf("%f",arr[a][b]);
      sum += arr[a][b];
      num++;
    }

  }
  float ave = sum/num;
  printf("%f", ave);
  
  for(int a =0; a < col; a++){
    int small =0; 
    for(int b =0; b< row; b++){
      if(arr[b][a]<ave){
        small++;
      }
    }
    fprintf(opf,"%d ",small);
  }
  fclose(opf);
  return 0;
}
