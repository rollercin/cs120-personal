#include <stdio.h>

int main() {

  printf("sizeof(int) [signed] = %lu bytes\n", sizeof(int));
  printf("sizeof(unsigned int) = %lu bytes\n", sizeof(unsigned int));
  printf("sizeof(double) [signed] = %lu bytes\n", sizeof(double));

  unsigned int big = 4294967295u;
  if (big+1 > big) {
    printf("\nbigger!\n");
  } else {
    printf("\nsmaller!\n");
  }

  return 0;
}

