#ifndef _BEN_CS120_TRANSCRIPT_HPP
#define _BEN_CS120_TRANSCRIPT_HPP

#include <string>
#include <list>
#include "course.hpp"

class Transcript {
public:
  Transcript();
  void addCourse(Course course);
  void printTranscript();
  double calculateGPA();
private:
  std::list<Course> courselist;
 };
  
#endif
