#include <iostream>
#include "course.hpp"
#include "transcript.hpp"
//#include "transcript.hpp" // you'll need to create this file

int main() {

  Course cs107("Intro Programming", 70, 3.0);
  Course cs120("Intermediate Programming", 90, 4.5);
  Course cs335("Artificial Intelligence", 80, 3.0);
  Course unknown;

  // check the first one:
  std::cout << "Course info: " << cs107.getName() << ", " 
                               << cs107.getGrade() << ", "
                               << cs107.getCredits() << "\n";

  // won't do anything until you finish the getCourseString() method
  std::cout << cs107.getCourseString() << "\n";
  std::cout << cs120.getCourseString() << "\n";
  std::cout << cs335.getCourseString() << "\n";
  std::cout << unknown.getCourseString() << "\n";


  Transcript myTranscript;
  myTranscript.addCourse(cs107);
  myTranscript.addCourse(cs120);
  myTranscript.addCourse(cs335);
  myTranscript.addCourse(unknown);

  std::cout << "Transcript: \n";
  myTranscript.printTranscript();
  std::cout << "GPA: " << myTranscript.calculateGPA() << "\n";


  return 0;
}
