#include <iostream>
#include <iterator>
#include "transcript.hpp"
#include "course.hpp"

Transcript::Transcript(){}

void Transcript:: addCourse(Course course){
  Transcript::courselist.push_back(course);
}

void Transcript:: printTranscript(){
  for(auto iter = courselist.begin(); iter != courselist.end(); iter++){
    std::cout << iter->getCourseString() << std::endl;
  }
}

double Transcript::calculateGPA(){
  double a = courselist.size();
  double total;
  for(std::list<Course>::iterator iter = courselist.begin(); iter != courselist.end(); iter++){
    total += iter->getGrade();
  }

  return  (total/a); 
}
