#include <iostream>
#include <iterator>
#include "course.hpp"

Course :: Course() : name("Default Coursename"), grade(0), credits(0) {
}

Course :: Course(std::string s, double g, double c) 
  : name(s)
{
  setGrade(g);
  setCredits(c);
}

std::string Course :: getName() const {
  return name;
}

void Course :: setName(std::string s) {
  name = s;
}

double Course :: getGrade() const {
  return grade;
}

void Course :: setGrade(double g) {
  if (g < 0 || g > 100) {
    std::cerr << "ERROR: invalid grade!\n";
    g = 0;
  }
  grade = g;
}

double Course :: getCredits() const {
  return credits;
}

void Course :: setCredits(double c) {
  if (c < 0 || c > 6) {
    std::cerr << "ERROR: invalid number of credits\n";
    c = 0;
  }
  credits = c;
}


std::string Course :: getCourseString() {
  std::string str;
  str = name  + ", " + std::to_string(grade) + ", " + std::to_string(credits);
  return str;
}



