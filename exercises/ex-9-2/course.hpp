#ifndef _BEN_CS120_COURSE_HPP
#define _BEN_CS120_COURSE_HPP

#include <string>
#include <list>

class Course {
  public:

    Course();
    Course(std::string s, double g, double c);

    std::string getName() const;
    void setName(std::string s);

    double getGrade() const;
    void setGrade(double g);

    double getCredits() const;
    void setCredits(double c);

    std::string getCourseString();

  private:
    std::string name;
    double grade;
    double credits;
};


  
#endif
