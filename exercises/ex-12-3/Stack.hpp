#ifndef _CS120_STACK_HPP
#define _CS120_STACK_HPP

#include <iostream>
#include <vector>

/* The Stack class should behave like we'd expect from a stack,
 * meaning it's a first-in-last-out datastructure.  Basically,
 * you can only add or remove things at the top of the stack.
 */
class Stack {

public:
  Stack(); //default constructor
  Stack(const Stack &other); //copy constructor
  Stack & operator= (const Stack &other); //assignment operator
  ~Stack(); //destructor
  
  void push(int i); // add a new number to the top of the stack
  int pop(); // remove top number from the stack and return it
  int top() const; // return the value of the top stack element, but don't remove it
  void clear(); // remove all numbers from the stack

  bool empty() const; // return true if the stack is empty
  size_t size() const; // return the number of elements in the stack
  void reserve(size_t len); //reallocate storage to have specified capacity
  
  void swap(size_t offset =1); // swap the top element with some element lower down (by default, swaps the top two elements)

  /* let the output operator have access to the data */
  friend std::ostream & operator<< (std::ostream &os, const Stack &s);

private:

  /* no longer using a vector to store the numbers...it's time to do
   * the memory management ourselves */

  int * data;        /* pointer to storage for stack elements */
  size_t dataLength; /* number of elements that can fit into the 
			currently allocated space in data array */
  size_t stackDepth; /* current depth of stack, 0 means empty;
			index of data array of last legitimate
			stack entry is stackDepth-1 */
};

#endif
