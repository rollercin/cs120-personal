#include "Stack.hpp"
#include <stdexcept>
#include <cassert>

/* constructor 
   Creates a new object with default capacity (dataLength)
   equal to 4 and a stackDepth equal to 0.  Allocates the
   size-dataLength int array that data will point to.
*/
Stack :: Stack() {
  //TODO: fill this in
  data = new int[4];
  dataLength = 4;
  stackDepth = 0;
}


/* reserve
   Change amount of space allocated for stack element storage.
   Can be use to "reserve" a given amount of space if you know how much you'll
   need (to avoid repeated re-allocating).  This is also used within this class
   to expand storage (by doubling its size) when we run out of space. 
*/
void Stack :: reserve(size_t len) {
  int* temp = new int[len];
  for(unsigned i = 0; i < len && i < stackDepth && i < dataLength; i++){
    temp[i] = data[i];
  }
  delete [] data;
  data = temp;
  dataLength = len;
  
  //TODO: fill this in
}


/* push
   Adds (a copy of) the new element to the top of the stack.  If array
   of stack items becomes completely full after item is added, this
   method calls reserve to double the size of the existing array
*/
void Stack :: push (int i) {
  if(stackDepth + 1 <= dataLength){
    data[stackDepth] = i;
    stackDepth++;
  }else{
    reserve(dataLength * 2);
    data[stackDepth] = i;
    stackDepth++;
  }
}


/* size
   Returns the number of items currently stored in the stack.
   Note that the return value here is unrelated to the capacity of the array.
*/
size_t Stack :: size() const {
  
    return stackDepth;
}


/* empty
   Return whether stack contains any data items or not.
*/
bool Stack :: empty() const {
  
  return (!stackDepth); //replace this dummy return value
}


/* top
   Returns (a copy of) the top stack element, or throws
   a std::range_error if stack is empty when called.
*/
int Stack :: top () const {
  if(stackDepth){
    return data[stackDepth-1];
  }else{
    throw std::range_error("stack is empty");
  }
}


/* pop
   Removes the top element from the stack, and returns its value.
   Throws std::underflow_error if stack is empty when called.
*/
int Stack :: pop () {
  if(stackDepth){
    int value = data[stackDepth-1];
    stackDepth -= 1;
    return value;
  }else{
    throw std::underflow_error("too less data");
}
  }


/* clear
   Removes everything from the stack (actually just changes stackDepth).
*/
void Stack :: clear() {
  stackDepth = 0;
}


/* output stream operator <<
   Outputs each item from the stack, one per line, into the given stream,
   beginning with the element at the top of the stack.
*/
std::ostream & operator<< (std::ostream &os, const Stack &s) {
  for (int i = s.stackDepth-1; i >= 0; i--){
    os << s.data[i] << "\n";
   }
  return os; 
}



/* swap (completed for you)
   Exchanges two elements of the stack.  When called without an argument,
   the default value means it swaps the top two elements.  This version
   also allows other elements to be swapped with the top element by passing
   in argument representing the depth of the item to swap with the top. 
*/
void Stack :: swap (size_t offset) {
  //don't try to swap elements that don't exist  
  if (offset < 1 || stackDepth < offset+1) {
    throw std::range_error("attempt to swap with insufficient elements");
  }
  //swap specified element values
  int tmp = data[stackDepth-1];
  data[stackDepth-1] = data[stackDepth-offset-1];
  data[stackDepth-offset-1] = tmp;
}


/* destructor 
   Deallocates anything left in this object that was allocated with "new".
*/
Stack :: ~Stack () {
  delete [] data;
}


/* copy constructor 
   Creates a new object based on data in other.  Need to write
   this ourselves because we're using dynamic memory allocation
   and want a "deep copy" of the array holding the stack items.
   (Otherwise, the automatically-created copy constructor would be fine.)
*/
Stack :: Stack (const Stack &other) {
  dataLength = other.dataLength;
  stackDepth = other.stackDepth;

  data = new int[dataLength];
  for(size_t i = 0; i < stackDepth; i++){
    data[i] = other.data[i];
  }
}


/* assignment operator
   Copies values from other into this object.  Need write our own operator
   because we use dynamic allocation in this class and want a "deep copy".
*/
Stack & Stack :: operator= (const Stack &other) {
  if(this == &other){ 
    return *this;
  }
  stackDepth = other.stackDepth;
  dataLength = other.dataLength;

  delete [] data;
  for(size_t i = 0; i < stackDepth; i++){
    data[i] = other.data[i];
  }
 return *this; //replace this dummy return value
}





