#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* struct to store an RGB pixel,
 * one byte per channel */
typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

/* struct to bundle together a pixel array 
 * with the other image data we'll 
 * frequently want to pass around with it
 * (this saves us from having to pass the same
 * three variables to every function)
 */
typedef struct _image {
  Pixel *data;
  int rows;
  int cols;
  int colors;
} Image;


/* function to write a PPM formatted image file to disk */
int writePPM(Pixel *image, int rows, int cols, int colors, FILE *fp);
/* wrapper that takes an Image struct */
int writePPMImage(Image *im, FILE *fp);
/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename);


int main() {
  // define what we want our image to be like
  int rows = 100;
  int cols = 200;
  Pixel myColor = {0, 127, 255};

  // allocate space for pixels
  Pixel *pix = malloc(sizeof(Pixel) * rows * cols);

  // fill image (set each pixels to the same color)
  for (int r=0; r<rows; ++r) {
    for (int c=0; c<cols; ++c) {
      pix[(r * cols) + c] = myColor;
    }
  }


  // write image to disk
  FILE *fp = fopen("test1.ppm", "wb");
  writePPM(pix, rows, cols, 255, fp);
  fclose(fp);

  // encapsulate in an Image struct
  Image im;
  im.data = pix;
  im.rows = cols; // oops, switched cols and rows...what happens?
  im.cols = rows;
  im.colors = 255;

  // write the Image struct
  fp = fopen("test2.ppm", "wb");
  writePPMImage(&im, fp);
  fclose(fp);

  // remember to free memory that you allocated!
  free(im.data);
  // could also do:
  // free(pix);

  return 0;
}


/* function to write PPM image to a filehandle */
int writePPM(Pixel *image, int rows, int cols, int colors, FILE *fp) {
  if (!fp) {
    fprintf(stderr, "Error:ppmIO - writePPM given a bad filehandle\n");
    return 0;
  }

  /* write tag */
  fprintf(fp, "P6\n");
  /* write dimensions */
  fprintf(fp, "%d %d\n%d\n", cols, rows, colors);
  /* write pixels */
  int written = fwrite(image, sizeof(Pixel), rows * cols, fp);
  if (written != (rows * cols)) {
    fprintf(stderr, "Error:ppmIO - failed to write data to file!\n");
  }

  return written;
}

/* wrapper that takes an Image struct */
int writePPMImage(Image *im, FILE *fp) {
  return writePPM(im->data, im->rows, im->cols, im->colors, fp);
}

/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename) {
  FILE *fp = fopen(filename, "wb");
  if (!fp) {
    fprintf(stderr, "Error:ppmIO - unable to open file \"%s\" for writing!\n", filename);
    return 0;
  }

  int written = writePPM(im->data, im->rows, im->cols, im->colors, fp);
  fclose(fp);

  return written;
}
