#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//takes in an array from stdin and prints out the value
int main(){
  int **arr2d = NULL;
  int row, col;
  scanf("%d",&row);
  scanf("%d",&col);
 
  arr2d = malloc(row*sizeof(int*));
  for(int i = 0; i < row; i++)
    {
      *(arr2d + i) = malloc(col*sizeof(int));
      int *prt = *(arr2d + i);
      for(int j = 0; j < col; j++)
	{
	  scanf("%d", prt+j);
	}
    }
 
  for(int a = 0; a < row; a++ )
    {
      for(int b = 0; b < col; b++)
	{
	  printf("%d ",arr2d[a][b]);
	}
      printf("\n");
    }
  return 0;
}
