#include<stdlib.h>
#include<stdio.h>

int main(){
  struct point{
    int x;
    int y;
  };
  struct point pt1 = {1,2};
  struct point pt2 = {3.4};

  struct rec {
    struct point a;
    struct point b;
  };

  struct rec screen = {pt1, pt2};
  printf("%d %d\n",screen.pt1.x,pt1.y);

  return 0;
}
