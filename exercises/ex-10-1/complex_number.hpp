#include <ostream>

class complex_number {

public:
  complex_number() { real = 0; imag = 0; };
  complex_number(double r, double i) { real = r; imag = i; }

  double getRealPart() const { return real; }
  void setRealPart(double r) { real = r; }

  double getImaginaryPart() const { return imag; }
  void setImaginaryPart(double i) { imag = i; }

  std::string toString() const;

  complex_number operator + (const complex_number &b)const;

  complex_number operator * (const complex_number &b)const;
  
private:
  double real;
  double imag;
};

//why the address of the operator 
std::ostream& operator << (std::ostream &os, const complex_number &b);
