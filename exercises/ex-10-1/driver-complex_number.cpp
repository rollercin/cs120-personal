#include <iostream>
#include "complex_number.hpp"

using std::cout;
using std::endl;

int main() {

  complex_number x;  //defaults to 0 + 0i
  complex_number y(2,3); //2 + 3i
  complex_number z(7,4); //7 + 4i

  cout << "x = " << x.toString() << endl;
  cout << "y = " << y.toString() << endl;
  cout << "z = " << z.toString() << endl << endl;






  cout << "y = " << x << " (using overloaded << to output it!)" << endl;
  cout << "new value = " << complex_number(8,6) << " (using overloaded << to output it!)" << endl << endl;

  complex_number sum = y + z;
  cout << "y + z = " << sum.toString() << " (using toString)" << endl;  
  cout << "y + z = " << sum << " (using overloaded <<)" << endl;
  cout << "y + y = " << (y + y) << " (using overloaded <<)" << endl << endl;







  complex_number product = y * z;
  cout << "y * z = " << product.toString() << " (using toString)" << endl;  
  cout << "y * z = " << product << " (using overloaded <<)" << endl;


  /* 
     CORRECT OUTPUT for Part 6 code above:

     y * z = 2 + 29i (using toString)
     y * z = 2 + 29i (using overloaded <<)
  */




  
  return 0;
}
