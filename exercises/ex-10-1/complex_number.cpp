#include <sstream>  //used by toString
#include <iomanip>  //used by toString
#include "complex_number.hpp"

//Returns a string representation of a complex number
std::string complex_number::toString() const {
  std::stringstream result;
  result << std::setprecision(3);
  result << real << " + " << imag << "i";
  return result.str();
}

complex_number complex_number::operator + (const complex_number&b)const{
  complex_number newnum;
  newnum.real = real + b.real;
  newnum.imag = imag + b.imag;
  return newnum;   
}

complex_number complex_number::operator * (const complex_number&b)const{
  complex_number newnum;
  newnum.real = real * b.real - imag * b.imag;
  newnum.imag = real * b.imag + imag * b.real;
  return newnum;
}

std::ostream& operator << (std::ostream &os, const complex_number&b){
  os<<b.toString();
  return os;
}
