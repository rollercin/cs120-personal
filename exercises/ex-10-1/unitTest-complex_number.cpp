#include "catch.hpp" // simple unit-testing framework
#include "complex_number.hpp" // the functions we want to test

#include <string>
#include <sstream>

using std::string;

TEST_CASE("getReal", "[getRealPart]") {
  complex_number b;
  complex_number c(5.2, 12.6);
  CHECK(b.getRealPart() == 0);
  CHECK(c.getRealPart() == 5.2);
}

TEST_CASE("setReal", "[setRealPart]") {
  complex_number c(5.2, 12.6);
  CHECK(c.getRealPart() == 5.2);
  CHECK(c.getImaginaryPart() == 12.6);
  c.setRealPart(33.1);
  CHECK(c.getRealPart() == 33.1);
  CHECK(c.getImaginaryPart() == 12.6);
  c.setRealPart(0);
  CHECK(c.getRealPart() == 0);
  CHECK(c.getImaginaryPart() == 12.6);
}

TEST_CASE("getImg", "[getImaginaryPart]"){
  complex_number b;
  complex_number c(5.2, 12.6);
  CHECK(b.getImaginaryPart() == 0);
  CHECK(c.getImaginaryPart() == 12.6);

}

TEST_CASE("setImg", "[setImaginaryPart]"){
  complex_number c(5.2, 12.6);
  CHECK(c.getRealPart() == 5.2);
  CHECK(c.getImaginaryPart() == 12.6);
  c.setImaginaryPart(33.1);
  CHECK(c.getRealPart() == 5.2);
  CHECK(c.getImaginaryPart() == 33.1);
  c.setImaginaryPart(0);
  CHECK(c.getRealPart() == 5.2);
  CHECK(c.getImaginaryPart() == 0);
 
}

TEST_CASE("toString","[toString]"){
  complex_number b;
  complex_number c(5.2, 12.6);
  complex_number d(1.2333, 12.3333);
  CHECK(b.toString() == "0 + 0i");
  CHECK(c.toString() == "5.2 + 12.6i");
  CHECK(d.toString() == "1.23 + 12.3i");
}


TEST_CASE("add","[operator +]"){
  complex_number b;
  complex_number c(5.2, 12.6);
  complex_number d(1.2, 12.3);
  b = c + d;
  CHECK(b.getRealPart() == 6.4);
  CHECK(b.getImaginaryPart() == 24.9);
  }



TEST_CASE("output stream operator <<", "[operator<<]") {
  complex_number b;
  complex_number c(5.2, 12.6);
  complex_number d(5.23495, 12.62271);
  std::stringstream ss;

  ss << b << ", " << c << ", " << d << "\n";
  CHECK(ss.str() == "0 + 0i, 5.2 + 12.6i, 5.23 + 12.6i\n");

  ss << complex_number(9.0, 3.2) << "\n";
  CHECK(ss.str() == "0 + 0i, 5.2 + 12.6i, 5.23 + 12.6i\n9 + 3.2i\n");
}
