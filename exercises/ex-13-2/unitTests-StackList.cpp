#include "StackList.hpp" // the functions we want to test

/* set up typename alias so we can re-use our old Stack unit tests
 * for both the Array version and the List version */
template <typename T>
using Stack = StackList<T>;

#include "unitTests-Stack.cpp" // the actual tests 
