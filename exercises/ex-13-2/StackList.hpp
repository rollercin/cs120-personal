#ifndef _CS120_STACK_LIST_HPP
#define _CS120_STACK_LIST_HPP
#include <iostream>
#include <string>
#include <sstream>
#include "StackInterface.hpp"


template <typename T>
class StackList : public StackInterface<T> {
  public:
    StackList() : head(nullptr), stackDepth(0) { } // default constructor
    StackList(const StackList<T> &other); // copy constructor; need to do deep copy!
    StackList<T> & operator= (const StackList<T> &other); // assignment operator (copy)
    ~StackList() { delete head; } // destructor

    /* remember, the "virtual" here is optional, unless we want to have some
     * other class extend StackList later.  It's in the parent class where the
     * "virtual" really matters */
    virtual std::string toString() const override;

   /****** stack size functions ******/
    virtual bool empty() const override { return stackDepth == 0; }
    virtual size_t size() const override { return stackDepth; }
    virtual void clear() override { stackDepth = 0; delete head; head = nullptr; }

    /****** stack element functions ******/
    //TODO: add five prototypes here
   void push(T i) override; // add a new element to the top of the stack
   T pop() override; // remove the top element from the stack, and return it
   void swap(size_t index =1) override; // swap two elements of the stack (by default, the top two)
   T & top() override; // return (a reference to) the top element of the stack
   const T & top() const override; // const version of top()
  

  
  private:
    /* private nested class for node type */
    class ListNode {
      friend StackList<T>; // friend outer class

      public:
      ListNode(T val, ListNode *nxt =nullptr) : data(val), next(nxt) { }
      ~ListNode() { delete next; }

      std::string toString() const;

      private:
      T data;
      ListNode *next;
    };

    /* private data members */
    ListNode *head;
    size_t stackDepth;

};

template <typename T>
StackList<T> :: StackList(const StackList<T> &other) {
  *this = other; // use assignment operator to do a deep copy
}

/* Assignment operator, copies values from other into
 * this.  Needs to be a deep copy due to dynamic
 * allocation. */
template <typename T>
StackList<T> & StackList<T> :: operator= (const StackList<T> &other) {

  //TODO: replace this stub
  return nullptr;
}



template <typename T>
std::string StackList<T> :: ListNode :: toString() const {
  std::stringstream ss;
  ss << data << "\n";
  if (next) {
    ss << next->toString();
  }
  return ss.str();
}


template <typename T>
std::string StackList<T> :: toString() const {
  if (head) {
    return head->toString();
  } else {
    return "";
  }
}


//TODO: add missing method bodies here
template <typename T>
void StackList<T> :: push(T i){
  ListNode *p = new ListNode(i,head);
  head = p;
  stackDepth++;
}

template <typename T>
T StackList<T> :: pop(){
  ListNode *temp = head->next;
  delete [] head;
  head = temp;
  stackDepth--;
}

template <typename T>
void StackList<T> :: swap(size_t index){

}

template <typename T>
T & StackList<T> :: top(){
  return head->data; 
}

template<typename T>
const T & StackList<T> :: top const(){
  return head->data;
}


#endif
