#ifndef _CS120_STACK_INTERFACE_HPP
#define _CS120_STACK_INTERFACE_HPP

#include <string>

/* abstract class intended to function as a Stack interface, allowing
 * different subclasses to implement the underlying functionality in different
 * ways 
 */
template <typename T>
class StackInterface {
  public:

    virtual std::string toString() const =0; // the =0 marks this function as pure virtual

    /****** stack size functions ******/
    virtual bool empty() const =0;
    virtual size_t size() const =0;
    virtual void clear() =0;

    /****** stack element functions ******/
    virtual void push(T i) = 0;
    virtual T pop() = 0;
    virtual void swap(size_t index =1) = 0;
    virtual T & top() = 0;
    virtual const T & top() const = 0;

};

template <typename T>
std::ostream & operator<< (std::ostream &os, const StackInterface<T> &st) {
  /* NOTE: since toString is pure virtual in StackInterface, this function
   * relies on dynamic binding and polymorphism.  Since you can't instanciate
   * an abstract class, and any sub-class is obligated to override the pure
   * virtual methods, this is actually perfectly safe.  There's no way to
   * call this function with an argument whos "true" type doesn't have a valid
   * toString() method
   */
  os << st.toString(); 
  return os; 
}






#endif
