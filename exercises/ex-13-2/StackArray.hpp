/***************************************************
 * StackArray.hpp
 * Ben Mitchell
 * 2015-11-29
 *
 * Templated stack class with nested-class iterators,
 * both const and non-const versions.
 ***************************************************/

#ifndef _BEN120_STACK_ARRAY_HPP
#define _BEN120_STACK_ARRAY_HPP

#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>
#include <cassert>
#include "StackInterface.hpp"

/* Basic stack class; similar to the one in the STL, but distinct
 * in a few ways, most notably that pop() actually returns the value
 * here (which the STL stack does not).
 *
 * Safely encapsulates dynamic memory allocation and addressing issues,
 * so the user gets a stack that will never overflow without having to
 * worry about the details.
 */
template <typename T>
class StackArray : public StackInterface<T> {
#if 0
  /* allow the output operator to access non-public members */
  template <typename U>
  friend std::ostream & operator<< (std::ostream &os, const StackArray<U> &s);
#endif

  public:
    /****** constructors/destructors ******/
    StackArray(); // default constructor
    StackArray(const StackArray<T> &other); // copy constructor
    StackArray<T> & operator= (const StackArray<T> &other); // assignment operator (copy)
    ~StackArray(); // destructor


    /****** stack string functions ******/
    std::string toString() const override;

    /****** stack size functions ******/
    bool empty() const  override{ return (stackDepth == 0); } // query whether stack is empty
    size_t size() const  override{ return stackDepth; } // query current stack depth
    size_t capacity() const { return dataLength; } // query allocated space
    void reserve(size_t len); // re-allocate data storage to have specified length
    void shrink_to_fit(); // auto re-size to just fit the current data
    void clear() override { stackDepth = 0; } // remove everything from the stack (just changes stackDepth)

    /****** stack element functions ******/
    void push(T i) override; // add a new element to the top of the stack
    T pop() override; // remove the top element from the stack, and return it
    void swap(size_t index =1) override; // swap two elements of the stack (by default, the top two)
    T & top() override; // return (a reference to) the top element of the stack
    const T & top() const override; // const version of top()

  protected:
    /* storage for stack elements */
    T *data;

    /* currently allocated space in data array 
     * (i.e. number of elements that can safely be
     * stored without overflow) */
    size_t dataLength;

    /* current stack depth (0 means empty);
     * index in data of last legitimate stack entry is
     * stackDepth-1 */
    size_t stackDepth;


#if 0
  /****** iterator stuff ******/
  public:
    /*** const_iterator class ***/
    class const_iterator {
      public:
        const_iterator(T *e =nullptr, const StackArray<T> *s =nullptr) : elementPtr(e), stackPtr(s) { }

        const T& operator*() const; // dereference
        const T* operator->() const; // arrow
        StackArray<T>::const_iterator operator++(int); // post-increment
        StackArray<T>::const_iterator & operator++(); // pre-increment
        bool operator==(const StackArray<T>::const_iterator &other) const; // equality
        bool operator!=(const StackArray<T>::const_iterator &other) const { return !(*this == other); } // inequality

      protected:
        T *elementPtr;
        const StackArray * const stackPtr;
    };

    /*** iterator class (non-const version) ***
     * Extends const_iterator class, since we can re-use most of it.
     * Only real difference is that we need a non-const version of the
     * dereference operator so it gives us a reference to the element
     * that can be modified */
    class iterator : public StackArray<T>::const_iterator {
      public:
        /* delegate to base class constructor */
        iterator(T *e =nullptr, StackArray<T> *s =nullptr) : const_iterator(e, s) { }

        /* NOTE: these methods overload, they do *not* override.
         * This is intentional; we've left off the const specifiers on purpose
         * here.  */
        T& operator*(); 
        T* operator->();
    };

    /*** StackArray methods to get iterators ***/
    StackArray<T>::const_iterator begin() const;
    StackArray<T>::const_iterator end() const;
    StackArray<T>::iterator begin();
    StackArray<T>::iterator end();

#endif
};


/* default constructor */
template <typename T>
StackArray<T> :: StackArray () {
  dataLength = 4; // default data length
  stackDepth = 0; // stack starts empty
  data = new T[dataLength]; // allocate default amount of space
}

/* copy constructor */
template <typename T>
StackArray<T> :: StackArray (const StackArray &other) {
  *this = other; // just use the assignment operator (below)
}

/* destructor */
template <typename T>
StackArray<T> :: ~StackArray () {
  /* we just need to make sure that we de-allocate anything
   * that we allocated with "new", which in this case is
   * the array pointed to by "data" */
  delete[] data; 
}


/* Assignment operator, copies values from other into
 * this.  Needs to be a deep copy due to dynamic
 * allocation. */
template <typename T>
StackArray<T> & StackArray<T> :: operator= (const StackArray<T> &other) {
  /* copy basic variables */
  dataLength = other.dataLength;
  stackDepth = other.stackDepth;
  /* de-allocate current array, and allocate a new one of
   * the correct size */
  delete[] data;
  data = new T[dataLength];

  /* copy in-use elements one at a time */
  for (size_t i=0; i<stackDepth; i++) {
    data[i] = other.data[i];
  }
  
  /* assignment operator returns assigned value (in this case,
   * a reference to ourselves). */
  return *this;
}


template <typename T>
std::string StackArray<T> :: toString() const {
  std::stringstream ss;
  for (int i=stackDepth-1; i >= 0; i--) {
    ss << data[i] << "\n";
  }
  return ss.str();
}

/* change amount of space allocated for stack element storage.
 * Can be used to "reserve" a given amount of space if you know
 * how much you'll need (so you don't have to keep on re-allocating).
 * Is also used internally to expand storage space when we run out.
 */
template <typename T>
void StackArray<T> :: reserve(size_t len) {
  T *tmp = data; // keep track of old data
  data = new T[len]; // allocate new storage
  for (size_t i=0; (i < len) && (i < dataLength) && (i < stackDepth); i++) {
    data[i] = tmp[i]; // copy all the elements we need from old to new
  }
  dataLength = len; // update length
  delete[] tmp; // de-allocate old storage
}

/* re-allocate storage to just fit the current stack size. Not likely
 * to be that useful in practice, but if the stack grows way too big,
 * this is a way of cutting it down without loosing data.
 */
template <typename T>
void StackArray<T> :: shrink_to_fit() {
  reserve(stackDepth+1);
}

/* push adds (a copy of) the new element to the top of the stack. */
template <typename T>
void StackArray<T> :: push (T i) {
  assert(stackDepth < dataLength); // shouldn't be possible, but check anyway

  data[stackDepth] = i; // copy new element into array
  stackDepth++; // update element count
  if (stackDepth == dataLength) { // if we just used up the last empty slot, make more
    reserve(dataLength * 2); // any time we run out of space, we double the length
  }
}

/* pop removes the top element from the stack, and returns its value */
template <typename T>
T StackArray<T> :: pop () { 
  /* if we try to pop an empty stack, throw an appropriate exception */
  if (stackDepth == 0) {
    throw std::underflow_error("attempt to pop from empty stack");
  }
  /* if it's safe, update the stackDepth counter and return the requested value.
   * Note that the order here is perfectly safe, since stackDepth is just the
   * end of the "in-use" part of the array, not the end of the array itself.
   * The allocated space doesn't get shortened here. */
  stackDepth--;
  return (data[stackDepth]);
}

/* swap exchanges two elements of the stack.  When called without
 * arguments, the default value means it swaps the top two elements
 * (this is the 'standard' way that swap works for stacks), but this
 * version also allows other elements to be swapped with the top
 * element by specifying the depth of the thing to swap with the top. */
template <typename T>
void StackArray<T> :: swap (size_t index) {
  /* if we're about to do something bad, throw an exception instead */
  if (index < 1 || stackDepth < index+1) {
    throw std::range_error("attempt to swap with insufficient elements");
  }
  /* swap specified element values */
  T tmp = data[stackDepth-1];
  data[stackDepth-1] = data[stackDepth-index-1];
  data[stackDepth-index-1] = tmp;
}

/* top returns (a reference to) the top stack element */
template <typename T>
T & StackArray<T> :: top () {
  /* if we're about to do something bad, throw an exception instead */
  if (stackDepth < 1) {
    throw std::range_error("top() called on empty stack");
  }
  /* return top stack value */
  return data[stackDepth-1];
}

/* const version of top; does the same thing, but is/returns const */
template <typename T>
const T & StackArray<T> :: top () const {
  /* if we're about to do something bad, throw an exception instead */
  if (stackDepth < 1) {
    throw std::range_error("top() called on empty stack");
  }
  /* return top stack value */
  return data[stackDepth-1];
}

#if 0
/* functions to get an iterator */
template <typename T>
typename StackArray<T>::iterator StackArray<T> :: begin() {
  return iterator(data+stackDepth-1, this);
}

template <typename T>
typename StackArray<T>::const_iterator StackArray<T> :: begin() const{
  return const_iterator(data+stackDepth-1, this);
}

template <typename T>
typename StackArray<T>::iterator StackArray<T> :: end() {
  return iterator(nullptr, this);
}

template <typename T>
typename StackArray<T>::const_iterator StackArray<T> :: end() const{
  return const_iterator(nullptr, this);
}


/* Iterator class functions */

/* dereference operator */
template <typename T>
const T& StackArray<T>::const_iterator :: operator*() const {
  // make sure things are safe (pointers are valid, we won't underflow)
  if (stackPtr == nullptr || elementPtr == nullptr) {
    throw std::range_error("Failed to dereference StackArray<>::iterator");
  }
  /* get the current element, return a reference to it */
  return *elementPtr;
}

/* arrow operator */
template <typename T>
const T* StackArray<T>::const_iterator :: operator->() const {
  if (stackPtr == nullptr || elementPtr == nullptr) {
    throw std::range_error("Failed to dereference StackArray<>::iterator");
  }
  /* here, we just return the pointer to the element; this will let the
   * user do something like iter->length(), assuming that whatever type T
   * is has a public method called length() */
  return elementPtr;
}

/* equality test */
template <typename T>
bool StackArray<T>::const_iterator :: operator==(const StackArray<T>::const_iterator &other) const {
  return (stackPtr == other.stackPtr && elementPtr == other.elementPtr);
}


template <typename T>
typename StackArray<T>::const_iterator & StackArray<T>::const_iterator :: operator++() {
  // make sure things are safe (pointers are valid, we won't underflow)
  if (stackPtr == nullptr || elementPtr == nullptr ) {
    throw std::range_error("Failed to increment StackArray<>::iterator");
  }
  if (elementPtr == stackPtr->data) { // if it's the bottom frame
    elementPtr = nullptr; // note that we've gone past the end
  } else {
    elementPtr--; // step down the stack
  }
  return *this;
}


template <typename T>
typename StackArray<T>::const_iterator StackArray<T>::const_iterator :: operator++(int) {
  /* just make a copy to return, then use the already defined increment operator */
  StackArray<T>::const_iterator old(*this);
  ++(*this);
  return old;
}

/*******
 * functions for non-const iterators
 * note that there is some minor code replication here; there are
 * ways around this, but they're messy enough that when the functions
 * are this short, it may be cleaner to leave the replication
 * (the alternatives may result in more code overall, as well as code
 * that's more confusing to read)
 */

/* non-const version of dereference (basically just like the other one,
 * but returns a non-const reference to the element)*/
template <typename T>
T& StackArray<T>::iterator :: operator*() {
  // make sure things are safe (pointers are valid, we won't underflow)
  if (const_iterator::stackPtr == nullptr || const_iterator::elementPtr == nullptr) {
    throw std::range_error("Failed to dereference StackArray<>::iterator");
  }
  /* note that we need to refer to the base-type members by full name;
   * alternatively, we could use this->elementPtr (see next function) */
  return *(const_iterator::elementPtr);
}


/* non-const arrow operator */
template <typename T>
T* StackArray<T>::iterator :: operator->() {
  if (this->stackPtr == nullptr || this->elementPtr == nullptr) {
    throw std::range_error("Failed to dereference StackArray<>::iterator");
  }
  return this->elementPtr;
}
#endif


/* overloaded output operator; not part of the class, but is a friend,
 * so we can access private variables. */
template <typename T>
std::ostream & operator<< (std::ostream &os, const StackArray<T> &st) {
#if 0
  /* loop through "in-use" part of the array, starting with the
   * last element (which is the top of the stack) and working
   * back towards the front (which is the bottom of the stack).
   * Prints linebreaks so the stack shows up vertically.
   */
  /*
  for (int i=st.stackDepth-1; i >= 0; i--) {
    os << st.data[i] << "\n";
  }
  */
  for (auto &i : st) {
    //++i;
    os << i << "\n";
  }
#endif
  os << st.toString();
  return os;
}



#endif
