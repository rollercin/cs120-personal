#include <stdio.h>
#include <stdlib.h>  
#include <assert.h>  


int* createArray(int size, int value);
void readArray(FILE *fp, int *newSize, int **newArray);
void printArray(int *array, int size);
int* my_realloc(int* ptr, int oldSize, int newSize);


int main(int argc, char* argv[]) {

  //Collect int values from command line to represent size and initialization value
  if (argc < 3) {
    printf("Too few command-line arguments.\nUsage: %s <size> <value>\n", argv[0]);
    return 1;
  }
  int size = atoi(argv[1]);
  int value = atoi(argv[2]);

  if (size < 1) {
    printf("Size argument %d too small.  Please run with size > 0.\n", size);
    return 1;
  }
  
  printf("Creating an array with %d elements all initialized to %d.\n\n", size, value);
  

  int* list = createArray(size, value);
  printArray(list, size);


  int list2Size = 0;
  int *list2 = NULL;
  readArray(stdin, &list2Size, &list2);
  printArray(list2,list2Size);  
 
  

 
  list = my_realloc(list, size, size*2);
  size *= 2;
  printArray(list, size);
  //Output all of the values in the array that was newly reallocated
  // TODO - Part 4: replace with function call when part 4 function works


  //TODO - all parts: Perform any final "clean-up" needed by this program




  free(list2);
  free(list);    
  return 0;
}



/*
  Function that returns an array of "size" int elements,
  where each position has been initalized to "value"
*/
int* createArray(int size, int value){

  //TODO - Part 2: Fill in function defintion here
  //  Since the code won't compile if we don't return
  //  something, just return a default value.  This is
  //  a "function stub", basically it's got the right
  //  prototype, and returns the right type, but doesn't
  //  actually do anything. It's enough to let us compile
  //  without errors, though.
  //
  //  You'll need to replace this with the actual function
  //  definition.
  int* array = malloc (size * sizeof(int));
  assert (array != NULL);
  for(int i = 0; i < size; i++)
    {
      array[i] = value; 
    }
  return array;

}


/*
  Function that returns a newly allocated array with values
  obtained from the given filehandle.  The first number
  read will be treated as the size of the new array, and
  the function will then allocate the array and attempt to
  fill it with the requested number of integers. 
*/
void readArray (FILE *fp, int* newSize, int** newArray) {   // why use double pointer here 

//TODO - Part 3: function stub, doesn't do anything, but makes sure
//  it reports that it hasn't done anything.
//  Fill in actual code.
//  Be sure to read from the filehandle using fscanf(); 
//  DO NOT just read from stdin using scanf().
  fscanf(fp,"%d",newSize);
  int* arr = malloc((*newSize)*sizeof(int));
  assert(arr != NULL);
  for(int i = 0; i < *newSize; i++)
    {
      fscanf(fp,"%d",(arr + i));
    }
  *newArray = arr;
  if(*newSize == 0)
    {
      fprintf(stderr,"nothing!\n");
    }
   }


void printArray(int *array, int size) {

  //TODO - Part 4: Fill in function defintion here
  if (array) {
    for (int i = 0; i < size; i++) {
      printf("%d ", *array);
      array++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }
}


int* my_realloc(int* oldPtr, int oldSize, int newSize) {
  
  //TODO - Part 5: Function stub, replace with real function.
  //  Doesn't do anything, returns a NULL pointer to indicate
  //  it didn't actually allocate more space
  //check if there is really any work to do
  if (newSize == oldSize) {
    return oldPtr;
  }

  //allocate new block
  int* newPtr = malloc(newSize * sizeof(int));
  assert (newPtr != NULL);

  //determine how much data to copy over
  int numEltsToCopy = oldSize;
  if (oldSize > newSize) {  //downsizing
    numEltsToCopy = newSize;
  }

  //perform copy of data
  for (int i = 0; i < numEltsToCopy; i++) {
    newPtr[i] = oldPtr[i];
  }

  //deallocate old black
  free(oldPtr);

  //return new block's address
  return newPtr;

}
