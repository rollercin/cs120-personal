#include <stdio.h>
//arguments are now pointers to pointers to int 
void arraySwap(int **left, int **right) { // left points to p1, right points to p2 
  int *tmp = *left;  // address of p1  
  *left = *right;   // the address left points to now is the address that right point to, so left poin
                    // to p2 
  *right = tmp;    // the address right points to is the address of p1, so right point to p1 
}

int main() {
  int ones[] = { 1, 1, 1 };
  int twos[] = { 2, 2, 2 };

  int *p1 = ones;
  int *p2 = twos;

  /* result should be:
   * "p1: [ 2 2 2 ]
   *  p2: [ 1 1 1 ]"
   */
  arraySwap(&p1, &p2); // passing in the address of p1 and p2

  printf("p1: [ ");
  for (int i=0; i<3; i++) {
    printf("%d ", p1[i]);
  }
  printf("]\n");

  printf("p2: [ ");
  for (int i=0; i<3; i++) {
    printf("%d ", p2[i]);
  }
  printf("]\n");

  return 0;
}
