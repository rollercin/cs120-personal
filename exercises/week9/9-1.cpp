#include<iostream>
#include<vector>

using namespace std;

template <typename T>
vector<T> readvec(){
  vector<T> vec;
  T val;
  cin >> val;
  while(!cin.fail() && !cin.eof()){
    vec.push_back(val);
    cin >> val;
 }
  cin.clear();
  cin.ignore(256, '\n');
  return vec;
}

template <typename T>
void printVec(vector<T> vec){
  for(T i : vec){
    cout << i << " ";
   }
  cout << endl;
}

template <typename T>
void calcS(vector <T> vec){
  T sum{};
  for(T i : vec){
    sum += i;
  }
  cout << sum << endl;
  
}


int main(){
  vector<string> v1;
  v1 = readvec<string>();
   printVec(v1);
   calcS(v1);
}
