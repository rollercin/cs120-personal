/* NgramCollection
 *
 * <Cindy Yang>
 * <11/14/2016>
 *
 * functions for NgramCollection class
 */
#include <iostream>
#include "../include/NgramCollection.hpp"

using std::stringstream;
using std::string;
using std::vector;
using std::map;
using std::tuple;
using std::get;

// functions that increment the count of each n-gram in the map 
void NgramCollection::increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end){

  vector<string> vec;
  for(unsigned i = 0; i < n-1; i++){
    vec.push_back(*(begin+i));
  }
  counts[vec][*(end-1)]++;
}

//determain which function to call
//default is a 
std::string NgramCollection::toString(char order) const{
  if (order == 'a') {
    return toStringAlpha();
  } else if(order == 'r') {
    return toStringReverseAlpha();
  } else if(order == 'c') {
    return toStringCount();
  }else{
    return "invalid order of input";
  }
}
// will call one of the below (defaults to alpha if no argument)
// specialized print-sorted functions
std::string NgramCollection::toStringAlpha() const {
  stringstream ss;    // stores the output into a stringstream
  for (auto it = counts.begin();    // traverse the map 
       it != counts.end();
       it++)
    {
      for (auto it2 = it->second.begin();   // traverse the map for each vector<string>
	   it2 != it->second.end();
	   it2++)
	{
	  for (string it3 : it->first){
	    ss << it3 << " ";
	  }
	  ss << it2->first << " " << it2->second << "\n";
	}
    }
  return ss.str();
}

std::string NgramCollection::toStringReverseAlpha() const {
  stringstream ss;
  //reverse the order of traversing 
  for (auto it = counts.rbegin();   
       it != counts.rend();
       it++)
    {
      for (auto  it2 = it->second.rbegin();    
	   it2 != it->second.rend();
	   it2++)
	{
	  for (string it3 : it->first){
	    ss << it3 << " ";
	  }
	  ss << it2->first << " " << it2->second <<"\n";
	}
    }
  return ss.str();
}
//sort the string based on their count 
std::string NgramCollection::toStringCount() const {
  stringstream ss;
  //freqCount is a tuple that rearranges the map into the order based on frequency
  vector<tuple<unsigned, vector<string>, string> > freqCount;
  for (auto it = counts.begin();
       it != counts.end();
       it++)
    {
      for (auto it2 = (it->second).begin();
	   it2 != (it->second).end();
	   it2++)
	{
	  freqCount.push_back(make_tuple(it2->second, it->first, it2->first));
	}
    }

  //sort the tuple based on frequency order
  sort(freqCount.begin(), freqCount.end());
  for(auto it = freqCount.begin();
      it != freqCount.end();
      it++)
    {
      for (string it2 : get<1>(*it)) {
	ss << it2 << " ";
      }
      ss << get<2>(*it) << " " << get<0>(*it) << '\n';
    }
  return ss.str();
}


std::string NgramCollection::pickWord(std::list<std::string>::const_iterator begin,
				      std::list<std::string>::const_iterator end) const {
  string nextWord;

  vector<string> vec;

  for(auto it = begin; it != end; it++){
    vec.push_back(*it);
  }

  int tot = 0;
  for (auto it = counts.at(vec).begin();
       it != counts.at(vec).end(); it++)
    {
      tot = tot + it->second;
    }
  assert(tot > 0);

  int index = rand() % tot;

  for (auto it2 = counts.at(vec).begin();
       it2 != counts.at(vec).end(); it2++)
    {
      tot = tot - it2->second;
      if (tot <= index) {
	nextWord = it2->first;
	break;
      }
    }
  return nextWord;
}


