/* LanguageModel.cpp
 *
 * <Cindy Yang>
 * <11/14/2016>
 *
 * functions for LanguageModel class
 */
#include <iostream>
#include <iterator>
#include "../include/LanguageModel.hpp"

using std::string;
using std::cout;
using std::endl;
using std::ifstream;
using std::vector;
using std::list;
using std::stringstream;
//read in a single file and add START AND END to its content 
vector<string> LanguageModel::readStr(string fname, unsigned n){
  ifstream myfile;
  myfile.open(fname);
  string word;
  vector<string> vec;

  if(myfile.eof()){
    std::cerr <<"error opening file" <<endl;
    exit(1);
  }

  for(unsigned i = 1; i < n ; i++){
    string s = std::to_string(i);
    string s1 = "<START_";
    string s2 = ">";
    string a = s1+s+s2;
    vec.push_back(a);
  }

  myfile >> word;
  while(!myfile.eof()){
      vec.push_back(word);
      word = "";
      myfile >> word;
  }

  for(unsigned i = 1; i < n ; i++){
    string s = std::to_string(i);
    string s1 = "<END_";
    string s2 = ">";
    string a = s1+s+s2;
    vec.push_back(a);
  }
  myfile.close();
  return vec;  
}

//read in a file contains file name
//then generate the Ngram map
void LanguageModel::generateMap(string filename, unsigned n){
  ifstream myfile;
  myfile.open(filename);
  string fname;
  vector<string> vec;

  if(myfile.eof()){
    std::cerr <<"error opening file" <<endl;
    exit(1);
  }
  myfile >> fname;
  while(!myfile.eof()){
     vector<string> vec = readStr(fname,n);
    for(auto it = vec.begin(); it < vec.end()+1-n; it++){
      coll.increment(it, it+n);
    }
    myfile >> fname;
    vec = {};
    }
  myfile.close();
 }  

//prints out Ngram map 
void LanguageModel::printModel(char c){
  cout << coll.toString(c);
}

//generateSentence based on the number of sentence in input, and n-gram
string LanguageModel::generateSentence(int numsen, int n){
  list<string> list;
  stringstream ss;  
  for(int num = 0; num < numsen; num++){   
  for(int i = 1; i < n ; i++){
    string s = std::to_string(i);
    string s1 = "<START_";
    string s2 = ">";
    string a = s1+s+s2;
    ss << a << " ";
      list.push_back(a);
  }
  string end = "<END_" + std::to_string(n-1) + ">";
  string next = coll.pickWord(list.begin(), list.end());
  while(next != end){
    list.pop_front();
    list.push_back(next);
    ss << next << " "; 
      next = coll.pickWord(list.begin(), list.end());
    }
   ss << end << endl;
  list = {};
  
  }
  return ss.str();
}
//accessor 
NgramCollection LanguageModel::getColl(){
  return coll;
}
