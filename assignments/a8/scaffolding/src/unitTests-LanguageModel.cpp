/*                                                                                                          
 * Unit test cases for NgramCollection class.                                                               
 */

#include "../include/catch.hpp" // simple unit-testing framework
#include <iostream>
#include "../include/LanguageModel.hpp" // header declaring the functions we want to test

#include <string>
#include <vector>
#include <list>

using std::stringstream;
using std::string;
using std::vector;
using std::list;

// test for function readStr
//it reads a single file and add START and END to the content
TEST_CASE("readStr", "[readStr]"){
  LanguageModel g3(3);
  vector<string> v = g3.readStr("../data/test1.txt", 3);
  stringstream ss;
  for(string it : v){
    ss << it << " ";
  }
  
  REQUIRE( ss.str()  == "<START_1> <START_2> tinker to evers to chance or maybe evers to chance to tinker for the double play, then take eight hours to sleep per night or maybe take eight hours to dream <END_1> <END_2> ");
  vector<string> v1 = g3.readStr("../data/test2.txt", 3);
  stringstream ss1;
  for(string it : v1){
    ss1 << it <<" ";
  }
  REQUIRE( ss1.str() == "<START_1> <START_2> I'd love the chance to tinker or to sleep per chance to dream <END_1> <END_2> ");
}

//test the function generateMap
TEST_CASE("generateMap", "[generateMap][alphabetical][bigram]"){
  LanguageModel g2(2);
  g2.generateMap("../data/file.txt", 2);
  NgramCollection coll = g2.getColl();
  REQUIRE(coll.toString() == "<START_1> I'd 1\n<START_1> tinker 1\nI'd love 1\nchance or 1\nchance to 3\ndouble play, 1\ndream <END_1> 2\neight hours 2\nevers to 2\nfor the 1\nhours to 2\nlove the 1\nmaybe evers 1\nmaybe take 1\nnight or 1\nor maybe 2\nor to 1\nper chance 1\nper night 1\nplay, then 1\nsleep per 2\ntake eight 2\nthe chance 1\nthe double 1\nthen take 1\ntinker for 1\ntinker or 1\ntinker to 1\nto chance 2\nto dream 2\nto evers 1\nto sleep 2\nto tinker 2\n");
}


//test case for generate sentence 
TEST_CASE("generateSentence","[generateSentence]"){
  LanguageModel g2(2);
  g2.generateMap("../data/file1.txt",2);
  string str1 ="<START_1> at here <END_1>\n";  //case1 with 0.5 probabiltiy
  string str2 ="<START_1> at at here <END_1>\n";  // case2 with 0.25 probability
  string result;
  unsigned int numTrials = 10000;
  unsigned int num1 = 0;
  unsigned int num2 = 0;

  //generate 10000 sentences and calculate the occurance of each case
  for (unsigned int i = 0; i < numTrials; i++) {
    result = g2.generateSentence(1,2);
    if (result == str1) {
      num1++;
    }
    else if (result == str2) {
      num2++;
    }
  }

  REQUIRE((num1 / (double) numTrials) == Approx(0.50).epsilon(0.03));    
  REQUIRE((num2 / (double) numTrials) == Approx(0.25).epsilon(0.03));
}
