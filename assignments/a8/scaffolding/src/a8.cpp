/* a8.cpp
 *
 * <Cindy Yang>
 * <11/14/2016>
 *
 * main function for the program
 */
#include <iostream>
#include "../include/LanguageModel.hpp"

using std::list;
using std::string;
using std::cout;
using std::endl;

int main(int argc, char** argv){
  //check the number of command line input
  if(argc < 4 || argc > 5){
    cout << "wrong number of input" << std::endl;
    return 0;
  }
  
  string fname = argv[1];     //read in the filename
  int n  = atoi(argv[2]);    //convert to int 
  char c = *(argv[3]);       //get a, r ,c or g

  //make sure Ngram is >=2
   if (n < 2) {
    std::cerr << "invalid Ngram input" << '\n';
    return 0;
  }

   // generate sentence 
   if(c == 'g'){
  LanguageModel model(n);
  int numsen = atoi(argv[4]);
  if(numsen < 0){
    std::cerr << "invalid scentence number" << '\n';
    return 0;
  }
  model.generateMap(fname, n);
  string sentences =  model.generateSentence(numsen, n);
  cout << sentences << endl;
  } else {                         
     LanguageModel model(n);             //print out language model
    model.generateMap(fname, n);
    model.printModel(c);
   }
}
