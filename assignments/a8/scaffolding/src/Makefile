###############################
# path variables
###############################
# where to look for .h files
INC_DIRS = ../include
# where to put .o files
OBJ_DIR = ./obj
# where to put executables
BIN_DIR = ../bin


# make sure the output directories exists...
$(shell mkdir -p $(OBJ_DIR))
$(shell mkdir -p $(BIN_DIR))

###############################
# targets
###############################

all: a8 test1 test2

tests: test1 test2

test1: $(OBJ_DIR)/unitTestDriver.o $(OBJ_DIR)/unitTests-NgramCollection.o $(OBJ_DIR)/NgramCollection.o
	g++ -g -lm $(OBJ_DIR)/unitTestDriver.o $(OBJ_DIR)/unitTests-NgramCollection.o $(OBJ_DIR)/NgramCollection.o -o $(BIN_DIR)/test1

test2: $(OBJ_DIR)/unitTestDriver.o $(OBJ_DIR)/NgramCollection.o $(OBJ_DIR)/unitTests-LanguageModel.o $(OBJ_DIR)/LanguageModel.o
	g++ -g -lm $(OBJ_DIR)/unitTestDriver.o $(OBJ_DIR)/unitTests-LanguageModel.o $(OBJ_DIR)/NgramCollection.o $(OBJ_DIR)/LanguageModel.o -o $(BIN_DIR)/test2

a8: $(OBJ_DIR)/a8.o $(OBJ_DIR)/LanguageModel.o $(OBJ_DIR)/NgramCollection.o
	g++ -g -lm $(OBJ_DIR)/a8.o $(OBJ_DIR)/LanguageModel.o $(OBJ_DIR)/NgramCollection.o -o $(BIN_DIR)/a8

$(OBJ_DIR)/unitTestDriver.o: unitTestDriver.cpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c unitTestDriver.cpp -o $(OBJ_DIR)/unitTestDriver.o

$(OBJ_DIR)/unitTests-LanguageModel.o: unitTests-LanguageModel.cpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c unitTests-LanguageModel.cpp -o $(OBJ_DIR)/unitTests-LanguageModel.o

$(OBJ_DIR)/unitTests-NgramCollection.o: unitTests-NgramCollection.cpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c unitTests-NgramCollection.cpp -o $(OBJ_DIR)/unitTests-NgramCollection.o

$(OBJ_DIR)/NgramCollection.o: NgramCollection.cpp $(INC_DIRS)/NgramCollection.hpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c NgramCollection.cpp -o $(OBJ_DIR)/NgramCollection.o

$(OBJ_DIR)/LanguageModel.o: LanguageModel.cpp $(INC_DIRS)/LanguageModel.hpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c LanguageModel.cpp -o $(OBJ_DIR)/LanguageModel.o

$(OBJ_DIR)/a8.o: a8.cpp
	g++ -Wall -Wextra -pedantic -std=c++11 -g -I../include/ -c a8.cpp -o $(OBJ_DIR)/a8.o


###############################
# "utility" rules
###############################
# the .PHONY line says these targets aren't expected to generate files named after the target
.PHONY: clean a8

# "shortcut" target (so we don't have to type the bin dir before our target name)
a6: $(BIN_DIR)/a8

# clean up generated files
clean:
	@echo removing generated files...
	@rm -f $(OBJ_DIR)/*.o $(BIN_DIR)/a8 $(BIN_DIR)/test 
