#ifndef _CS120_LANGUAGE_MODEL_HPP
#define _CS120_LANGUAGE_MODEL_HPP
#include <iostream>
#include <fstream>
#include "NgramCollection.hpp"

class LanguageModel{

public:
  LanguageModel(unsigned N): coll(N) {
  };
  std::vector<std::string> readStr(std::string fname, unsigned n);
  void generateMap(std::string filename, unsigned n);
  void printModel(char c);
  std::string generateSentence(int numsen, int n);
  NgramCollection getColl();
private:
  NgramCollection coll;
  
};
#endif
