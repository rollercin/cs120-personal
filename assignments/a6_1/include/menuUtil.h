#include <stdio.h>
#include "imageManip.h"

int displayMenu();
int interpret(char input[], Image* image);
int getInput(char word[]);
int handleInput();
