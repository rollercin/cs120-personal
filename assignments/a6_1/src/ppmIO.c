#include "ppmIO.h"

int readImage(char file[], Image* image) {
  FILE* fp = fopen(file, "rb");
  char type[2];
  int row;
  int col;
  int colors;
  fscanf(fp, "%s", type);    
  fscanf(fp, "%d" , &col);
  fscanf(fp, "%d", &row);
  fscanf(fp, "%d", &colors);
  fgetc(fp);
  Pixel* pixels = malloc(row * col * sizeof(Pixel));
  fread(pixels, sizeof(Pixel), row * col, fp);
  (*image).pixels = pixels;
  (*image).row = row;
  (*image).col = col;
  fclose(fp);
  return 0;
}

int writeImage(char file[], Image* image) {

  FILE* fp = fopen(file, "wb");
  fprintf(fp, "%s\n%d %d\n%d\n", "P6", (*image).col, (*image).row, 255);
  fwrite((*image).pixels, sizeof(Pixel), (*image).row * (*image).col, fp);
  fclose(fp);
  return 0;
}

