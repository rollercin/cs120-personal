/* wordsearch.c
 *
 * Cindy Yang 
 * <10/3/2016>
 *
 * Functions needed for a word-search puzzle solver.
 *
 * Scaffolding for CS120-F16, A4 - make at least 4 functions
 */
#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<stdlib.h>
#include "wordsearch.h"
// get characters from the input file, and put it into a string with max length 11
// truncate the word if it exceeds the length
//call wordS() function to find the word after gets the string
int getWord(char wstr[WORD_BUFFER_LENGTH], int sl, int row, char grid[MAX_GRID_SIZE][MAX_GRID_SIZE])
{ char a;
  int b = 0;
  while((a = getchar()) != EOF)  
    { if(isspace(a))     // get a string of word every time it runs into a space or new line 
	{
	  wstr[b] = '\0';
	  sl = strlen(wstr);
	 
	  if(!wordS(grid, wstr, row, sl))  //call wordS() function to find word
	    {
	      printf("\"%s\" - Not Found\n",wstr);   // print not fint when wordS returns 0 
	    }                                        
	  b = 0; 
	}
      else
	{ if(b < 10)
	    {wstr[b++] = a;}   // only put characters into string when its smaller than 10
	  
	}
    }
  return 0;
}
//read characters int the gird[][] from the puzzle file
//and returns zero when the input is invalid that is:
//if the length of the rows are not equal
//if the number of rows and columns are not equal
//if the length of row or column exceeds MAX_GRID_SIZE
int readgrid(char infname[], char grid[MAX_GRID_SIZE][MAX_GRID_SIZE])
{
  char c;
  int i = 0; int j = 0;int nc = 0; int t= 0;  // "nc" number of column
  FILE* inf = fopen(infname, "r");
  while((c = fgetc(inf)) != EOF)
    {
      if(isspace(c))
	{
	  if(c == '\n')
	    {
	      i++;
	      if(!t){        
		nc = j;
		t++;
	      }else{
		if(nc != j){
		  return 0;
		}
	      }

	      j = 0;
	    }
	  else
	    {
	      fclose(inf);
	     
	      return 0;
	    }
	}
      else
	{
	  grid[i][j] = c;     
	  j++;
	}
    }
  fclose(inf);
  if(i != nc || i > 10 || j > 10)   
    {
     
      return 0;
    }
  else
    {
      return nc;
    }

}
//search the wordstring(wstr)in the grid[][] 
//call direcS() function after it find the first letter of the word  
int wordS(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], char wstr[WORD_BUFFER_LENGTH], int row, int sl){
  int current = 0;   //current location in the string  
  int direc = 0;    
  int find = 0;     //0 when not found, 1 when found
  for(int cur = 0; cur < row; cur++){
    for(int cuc = 0; cuc < row; cuc++){
      if(grid[cur][cuc]== wstr[current]){
	while(direc < 4){
	  if(!direcS(grid, wstr, cur , cuc, sl, direc))
	    { direc++ ;}
	  else
	    { find = 1;
	      if(direc == 0)
		{printf("Found \"%s\" at %d %d, U\n",wstr, cur, cuc);}
	      else if(direc == 1)
		{printf("Found \"%s\" at %d %d, R\n",wstr, cur, cuc);}
	      else if(direc == 2)
		{printf("Found \"%s\" at %d %d, D\n",wstr, cur, cuc);}
	      else if(direc == 3)
		{printf("Found \"%s\" at %d %d, L\n",wstr, cur, cuc);}
	      direc++;
	    }
	}
	direc = 0;
      }
     
    }

  }
  return find;
}
//search for the direction of the word in the puzzle
//after finds the first letter of the word in wordS
int direcS(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], char wstr[WORD_BUFFER_LENGTH], int cur, int cuc, int sl, int direc)
{ int rplus = 0;
  int cplus = 0;
  if(direc == 0){rplus = -1; cplus = 0;}
  else if(direc == 1){rplus = 0; cplus = 1;}
  else if(direc == 2){rplus = 1; cplus = 0;}
  else if(direc == 3){rplus = 0; cplus = -1;}
  for(int a = 1; a < sl; a++)
    { if(grid[cur += rplus][cuc += cplus] != wstr[a])
	{return 0;}
    }
  return 1;
}
 
