/**************************************
 * a4.c 
 *
 * <Cindy Yang>
 * <10/3/2016>
 *
 * Program for solving word search puzzles.
 *
 *
 * Scaffolding for CS120-F16, HW4 - fill in
 * main, but keep it short; my reference implementation
 * has about 20 lines of actual code in main (not counting
 * comments and whitespace).
 *************************************/

#include <stdio.h>
#include "wordsearch.h"
#include<ctype.h>
#include<string.h>
#include<stdlib.h>
int main (int argc, char* argv[]) {
  int sl = WORD_BUFFER_LENGTH ;  //define the length of the input string
  int numArgu = argc;
  if(numArgu != 2){                     // print wronginput and terminate the program if the number
    fprintf(stderr, "wrongInput!\n");   // if the number of argument on command line is not 2
  }
  char grid[MAX_GRID_SIZE][MAX_GRID_SIZE];  //define the matrix 
  int row  = readgrid(argv[1], grid);  //read puzzle and return the width of the matrix
  if(row == 0){
    fprintf(stderr,"Invalid puzzle!\n");
    return 0;
  }
  char wstr[WORD_BUFFER_LENGTH];
  getWord(wstr, sl, row, grid);  //get the word from the input file and find the word
  return 0;
}
