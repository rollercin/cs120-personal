/* wordsearch.h
 *
 * <Cindy Yang>
 * <10/3/2016>
 *
 * Headers for functions needed for a word-search puzzle solver
 *
 * Scaffolding for CS120-F16, A4 - put function prototypes here
 */

#ifndef _CS120_SCAFFOLD_WORDSEARCH_H
#define _CS120_SCAFFOLD_WORDSEARCH_H

#include <stdio.h> // you'll need this if you want file handles as arguments 

#define MAX_GRID_SIZE 10 // constant for max grid size
#define WORD_BUFFER_LENGTH 11 // space for 10 "word" chars plus a null-termination

#endif

int getWord(char wstr[WORD_BUFFER_LENGTH], int sl, int row, char grid[MAX_GRID_SIZE][MAX_GRID_SIZE]);
int readgrid(char infname[], char grid[MAX_GRID_SIZE][MAX_GRID_SIZE]);
int wordS(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], char wstr[WORD_BUFFER_LENGTH], int row, int sl);
int direcS(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], char wstr[WORD_BUFFER_LENGTH], int cur, int cuc, int sl, int direc);
