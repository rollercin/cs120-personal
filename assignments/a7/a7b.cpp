/* a7b.cpp
 *
 * <Cindy Yang>
 * <11/6/2016>
 */
#include "a7.hpp"

//read the ipnut into a map that maps a pair of words to their count 
map<pair<string, string>, int> readmap(){
  map<pair<string, string>, int> mymap;    // create an map
  pair<string, string> temp1;   //create a pair 
  string temp2;                //string that store the integer 
  cin >> temp1.first;          // read in the first line 
  cin >> temp1.second;
  cin >> temp2;
  while (!cin.eof()){     //check if reaches the end of file 
    mymap[temp1] = stoi(temp2);    //map the pair of words to its count, have to convert from string to integer 
    cin >> temp1.first;     //keep reading 
    cin >> temp1.second;
    cin >> temp2;
  }
  return mymap;
}

//function that figures out which word goes next and print it out
//newmap is the map that contains all the pair with same first part and their count 
pair<string, string> getNext(map<pair<string,string>,int> newmap){  
  int total = 0;               //total tracks the total number of pairs with the same first part     
  pair<string, string> next;    //next is a pair of string whose second part is what we want to
                                //use as the start of next pair of word 
  for(auto it = newmap.begin(); it != newmap.end(); it++){  //traverse the map and add up these pairs
    total += it->second;                                    //total occurance 
  }                                                                   
  int index = rand()%total + 1;    //randomly generate a number form 1 to total, inclusive 
  for(auto it = newmap.begin(); it != newmap.end(); it++){    // traverse newmap
    if(index - it->second <= 0){   //if int minus the current maps count is less than zero  
      cout << it->first.second << " ";   //which means index is in the range of that count  
      next = it->first;                 // print out the second word in the pair   
      break;                          
    }else{
      index = index - it->second;     // if its bigger than zore, which means index is not in the range 
    }                                 // so subtract the count form the index 
  }
  return next;                     
}

//function that prints out the whole sentence 
void getSen(map<pair<string,string>,int> mymap){
  pair<string, string>  temp;
  for(auto it = mymap.begin(); it != mymap.end(); it++){  //traverse the map and print out <START> and
                                                          //the first word 
    if(it->first.first == "<START>"){
      cout << it->first.first << " " << it->first.second << " ";
      temp = it->first;          // temp is the first two word in the scentence 
      break;
    }
  }
  while(temp.second != "<END>"){   //keep finding the next word until the second word of the current pair is <END>
                                   //temp.second is the word we are using to find the word after it 
    map<pair<string, string>,int> newmap;      
    for(auto it = mymap.begin(); it != mymap.end(); it++){  // traverse the map and put all the pair  
      if(it->first.first == temp.second){    //with the same first element as temp.second 
        newmap[it->first] = it->second;      //into a newmap 
      } 
    }
    if(newmap.size() == 1){   //if the size of newmap is 1, which means there's only one possibility of the word 
      cout << (newmap.begin())->first.second << " "; //after temp.second 
      temp = (newmap.begin())->first;     //print out that word and set temp euqals to the next bigram
    }else{
      temp = getNext(newmap);    //if there's more than one element call getNext function; 
                                 //and set temp euqals to the next bigram 
    }
  }
}


int main(int argc, char** argv){
  srand(time(NULL));    //set up randon number generator 
  if(argc != 2){        //check the numebr of command line argument 
    cout << "wrong numbers of input!" << endl;
    return 0;
  }
  map<pair<string,string>, int> mymap;    
  mymap = readmap();    // read in the map 
  for(int i = 0; i < stoi(argv[1]); i++ ){  //generate sentences based on the number we input in the command line 
    getSen(mymap);
    cout << endl;
  }
 }
