/* wordsearch.h
 *
 * <Cindy Yang>
 * <11/6/2016>
 *
 * Headers for functions needed for both a7a and a7b
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::pair;
using std::map;
using std::endl;
using std::stoi;
