/*a7a.cpp
 *Cindy Yang xyang73 
 * <11/5/2016>
 *a7a
 */

#include"a7.hpp"

//functon that reads the input sentence and add <START> and <END> to it 
vector<string> readIn(){
  vector<string> vec;   //vector of string 
  vec.push_back("<START>");   //add <START>
  string word;          //store every word that read in; 
  cin >> word;
  while(!cin.eof()){          // add words to the string one at a time while not hitting the end of file
    vec.push_back(word);   
    cin >> word;
  }
  vec.push_back("<END>");
  return vec;
}

//construct a vector of pairs that pairs up every two consecutive words and sort them in alphebatical order 
vector<pair<string,string>> readBigram(vector<string> vec){
  vector<pair<string,string>> bgram;     // bgram: vector of pairs 
  for(size_t i = 0; i < vec.size()-1; i++){      // put every word in the sentence into pairs 
    bgram.push_back(make_pair(vec.at(i), vec.at(i+1)));
  }
  sort(bgram.begin(),bgram.end());    // sort the pairs in alphebatical order 
  return bgram;
}

//construct a map that maps the pair of words to their count  
map<pair<string,string>, int> getmap(vector<pair<string,string>> bgram){
  map<pair<string,string>, int> mymap = {};      // construct an empty map 
  for(size_t i = 0; i < bgram.size(); i++){     // loop through the vector of pairs 
    if(mymap.count(bgram[i]) == 0){             // if its the first time appear in the map, add it in    
       mymap[bgram[i]] = 1;
           }
     else{
       mymap[bgram[i]] += 1;       // if not the first time increase the counts 
     }
   }

  return mymap;
}

//print out the output based on "a", "r", or "c"
void getOutput(string a, map<pair<string, string>, int> mymap){
  if( a == "a"){    // if "a" print in order 
    for(auto it = mymap.begin(); it != mymap.end(); it++){
      cout << it->first.first << " " << it->first.second << " " << it->second << endl;
    } 
  }else if( a == "r"){   //if "r" reverse print the map
    for(auto it = mymap.rbegin(); it != mymap.rend(); it++){
      cout << it->first.first << " " << it->first.second << " " << it->second << endl;
    }
  }else if( a == "c" ){   //if "c"
    while(mymap.size()>0){    //find the smallest one in the map first print it out and erase it from the map
      pair<string,string> min;   //min : keep track of the pair with the smallest count at alphebatical order in the map
      min= (mymap.begin())->first;   
      for(auto it = mymap.begin(); it != mymap.end(); it++){  // traverse the map and let min equals to the pair that smaller than 
	if(it->second < mymap[min]){                         // the current min 
	  min = it->first;
	}else if(it->second == mymap[min]){    //if the count is same, check the alphabetical order 
	  if(it->first < min){
	    min = it->first;
	  }
	}
      }
      cout << min.first << " " << min.second << " " << mymap[min] << endl;  //print out min 
      mymap.erase(min);        //erase the smallest from the map

    }}else{
    cout << "invalid input" << endl;   //error if the input is something else 
  }

}

int main(int argc, char **argv){
  if (argc != 2){  //check the number of command line input
    cout << "wrong number of input!" << endl;    
    return 0;
  }
  vector<string> vec1;    // vector of strings, store the sentence    
  vector<pair<string,string>> bgram;   // vector of pairs 
  map<pair<string,string>, int> mymap;   // map the bigram to their count 
  vec1 = readIn();
  bgram = readBigram(vec1);
  mymap = getmap(bgram); 
  string a = argv[1];   
  getOutput(a,mymap);  
}
  

