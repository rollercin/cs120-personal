/*imageManip.c                                                                                                      
 *Cindy Yang                                                                                                
 *Varun Radhakrishnan                                                                                       
 * <10/16/2016>                                                                                             
 * functions needed for manipulation of images                                                                                             
 */

#include "../include/imageManip.h"
//first specify two cornors upper left and lower right
//then crop the image based on the differences between the two
int crop(Image* image, int x, int y, int x2, int y2) {
  if (y2  <  y || y2 > (*image).row || y < 0) {    //checks if the coordinate provided are invalid 
    printf("Coordinates are wrong\n");
    return 1;
  }
  if (x2 < x || x2 > (*image).col || x < 0) {
    printf("Coordinates are wrong\n");
    return 1;
  }
  int row = y2 - y;      //new row       
  int col = x2 - x;      //new col 
  Pixel* newPixels = malloc(sizeof(Pixel) * row * col); //allocate memory for new image 
  for (int r = y; r<y2; r++) {                 // copy new pixel from the old pixel based on the new size
    for (int c = x; c < x2; c++) {             // of the image 
      newPixels[(r - y) * col + (c - x)] = (*image).pixels[r * (*image).col + c];
    }
  }
  free((*image).pixels);               //free memory allocated 
  (*image).pixels = newPixels;         // put updated pixel, row and col into image 
  (*image).row = row;
  (*image).col = col;
  return 0;
}
//invert each color value
int inversion(Image* image) {
  for (int r = 0; r<(*image).row; r++) {       // traverse all the pixels and invert each colors color value 
    for (int c = 0; c<(*image).col; c++) {
      (*image).pixels[r * (*image).col + c].red = (*image).colors - (*image).pixels[r * (*image).col + c].red;
      (*image).pixels[r * (*image).col + c].green = (*image).colors - (*image).pixels[r * (*image).col + c].green;
      (*image).pixels[r * (*image).col + c].blue = (*image).colors - (*image).pixels[r * (*image).col + c].blue;
    }
  }
  return 0;
}
//copy each color component's value to the next color component 
int swap(Image* image) {

  for (int r = 0; r<(*image).row; r++) {     // traverse all the pixels
     for (int c = 0; c<(*image).col; c++) {
      unsigned char temp = (*image).pixels[r * (*image).col + c].red; //put the color component of red in temp  
      (*image).pixels[r * (*image).col + c].red = (*image).pixels[r * (*image).col + c].green;  // green -- > red  
      (*image).pixels[r * (*image).col + c].green = (*image).pixels[r * (*image).col + c].blue; // blue -- > green 
      (*image).pixels[r * (*image).col + c].blue = temp;   // red -- > blue 
    }
  }

  return 0; 
    
}
