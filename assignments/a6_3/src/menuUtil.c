/*menuUtil.c                                                                                                      
 *Cindy Yang                                                                                                
 *Varun Radhakrishnan                                                                                       
 * <10/16/2016>                                                                                             
 * contain fucntions that prints out menu and read in user input                                                                                                                                                                            
 */
#include "../include/menuUtil.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

// print out the menu
int displayMenu() {
  printf("%s" , "Main menu:\n");
  printf("%s" , "\tr <filename> - read image from <filename>\n");
  printf("%s" , "\tw <filename> - write image to <filename>\n");
  printf("%s" , "\tc <x1> <y1> <x2> <y2> -  crop image to the box with the given corners\n");
  printf("%s" , "\ts - swap channels\n");
  printf("%s" , "\ti - invert intensities\n");
  printf("%s" , "\tg - convert to grayscale\n");
  printf("%s" , "\tbl <sigma> Gaussian blur with the given radius (sigma)\n");
  printf("%s" , "\tsh <sigma> <amt> - sharphen by given amount (intensity), with radius (sigma)\n");
  printf("%s" , "\tbr <amt> - change brightness (up or down) by the given amount\n");
  printf("%s" , "\tcn <amt> - change contrast (up or down) by the given amount\n");
  printf("%s" , "\tq - quit\n");
  return 0;


}

//determine which function to call based on the characters in params
// params: a matrix of pointer points to each component of input separated by space
// image: image struct
// size : size of the params 
int interpret(char ** params, Image* image, int size) {
  if (size == 0 || strlen(params[0]) > 2) {   // no input   
    printf("Invalid Input\n");
    return 0;
  }
  switch(params[0][0]) {     

  case 'r' : {    // if the first character of the first string is r, print the second string which contains the address of
                  // the image file and call readImage()function  
    if (size == 2 && strlen(params[0]) == 1) {
      printf("Reading image at %s\n" , params[1]);
      readImage(params[1] , image);
    }
    else {         // invalid if there's more than two strings of input and the length of the first string('r') is not 1
      printf("Invalid Input: Please try again\n");
    }
    
  }
    break;

  case 'w' : {    // if the first character of the first string is w, print the second string which contains the address of
                  // the output file and call writeImage()function               
    if ((*image).pixels != NULL && size == 2 && strlen(params[0]) == 1)  {
      printf("Writing image at %s\n" , params[1]);
      writeImage(params[1], image);
    }
    else {  // invalide input if the pixels is not pointing to anything(haven't read anything in), there are more then two
            //strings of input and the length of the first string('w') is not 1
      printf("Please specify the file\n");
    }
  }
    break;

  case 'c' : {   //if the first character of the first string is c     
    if (strlen(params[0]) == 2 && params[0][1] == 'n') {   // check if it's 'cn'
      printf("Feature not yet implemented\n");
      return 0;
    }
    if (strlen(params[0]) != 1) {   // check if there's more than one character in the first string 
      printf("Invalid input");
      return 0;
    }
    printf("Cropping image\n");   
    if (size != 5) {         // if there's more than five input string, print 'invalid'
      printf("Invalid input\n");
      return 0;
    }
    if ((*image).pixels == NULL) {   //check if we have read in the image or not  
      printf("Please load an image\n");
      return 0;
    }
    int isValid = 1;  // converting strings of numbers to integer
                      // check if the string contain all numbers 
    for (int i = 1; i < size; i++) {
      if (!atoi(params[i])) {
	isValid = 0;     // set invalid 0, if there's character other than num 
      }
    }

    if (isValid) {    // call crop if input is valid       
      crop(image, atoi(params[1]), atoi(params[2]), atoi(params[3]), atoi(params[4]));
    }
    else {
      printf("Invalid Input\n");
    }
  }
    break;

  case 'i' : {  //if the first character of the first string is i
                
   printf("Inverting image\n");
   if (strlen(params[0]) > 1) {    // check if the first string has more then one character 
     printf("Invalid input\n");
     return 0;
   }
   if ((*image).pixels != NULL && size == 1) {   // check if we have read in the image, and is there's more
                                                 //than one string of input
     inversion(image);                         
   }
   else {
     printf("Please read a file\n");
   }
  }
   break;

  case 's' : {     //if the first character of the first string is s
    if (strlen(params[0]) == 2 && params[0][1] == 'h') {  // check if its 's' ot 'sh'
     printf("Feature not yet implemented\n");
     return 0;
    }
    else if (strlen(params[0]) != 1) {  // check if the input string contains characters other than s 
      printf("Invalid input\n");
      return 0;
    }
   printf("Swapping channels\n");     
   if ((*image).pixels != NULL && size == 1) {  // check if we have read in the image, and is there's more
                                                //than one string of input
     swap(image);
   }
   else {
     printf("Please read an image\n");
   }
  }
    break;
    
  case 'g' : {    //if the first character of the first string is g
    printf("Feature not yet implemented\n");
    return 0;
  }

    break;
  case 'b' : {     //if the first character of the first string is b
    if (strlen(params[0]) == 2 && params[0][1] == 'l') {
      printf("Feature not yet implemented\n");
      return 0;
    }
    else if(strlen(params[0]) == 2 && params[0][1] == 'r') {
      printf("Feature not yet implemented\n");
      return 0;
    }
  }
    break;
  case 'q' : {    //if the first character of the first string is q
   printf("GOOD BYE\n");  
   return 1;   // quit the menu and print it again  
  }
    break;
  default : {   
    printf("Invalid Input\n");  // case when there is no input 
  }
 }
 return 0;
}

// read user input into an array of strings  
char** getInput(int* n) {   //n is the size of the array
  int counter = 0;         // counter tracks the string length of each string 
  int wordCounter = 0;     //wordCouter tracks the number of strings 
  char c = NULL;         
  int capacity = 10;       // set the initial length of each string to 10 
  char** word = malloc(sizeof(char*) * 5);  // allocate memory of five character pointers to the array 
 
  for (int i = 0; i < 5; i++) {        // allocate memory for each string in the array 
    *(word + i) = malloc(sizeof(char) * capacity);
  }
  // keep reading in characters until '\n'
  while (c != '\n') {   

    if (wordCounter >= 5) {    // check if the size of the array is bigger than 5 
      printf("%s" , "Too many parameters: Try again\n");
      for(int i = 0; i < 5; i++) {
	free(*(word + i));   // free each string 
      } 
      free(word);     // free the array 
      return NULL;
    }
    	
    while(!isspace(c=getchar())) {    

      *(*(word + wordCounter) + counter) = c;  // put characters into word[wordCounter][counter]
      counter++;                               // let counter points to the next index in the string 

      if (counter >= capacity) {               //reallocate memory for the string if it exceeds the capacity     
	*(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * 2 * capacity);
	capacity = 2 * capacity;
      }
    }
    //reallocate memory for the string make sure the size is exact  
    *(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * (counter + 1)); 
    *(*(word + wordCounter) + counter) = '\0';  // add the null terminator to each string 
    
    wordCounter++;  // read next string in the array 

    
    counter = 0;     //reset counter and capacity 
    capacity = 10;
  }

  //free the memory for each string 
  for (int i = wordCounter; i < 5; i++) {   
    free(*(word + i));
  }
  //free the memory for the array 
  word = realloc(word, (wordCounter) * sizeof(char*)); // reallocate memory for the array based on the
                                                       // number of strings we read in 
  *n = wordCounter;   // set the size of the array to the number of string we actually read in 
  return word;       // return the array 
}

//handles the user input and calls other helper functions 
int handleInput() {
  Image image;
  image.pixels = NULL;  // initialize pixels to NULL 
  displayMenu();   // prints out menu 
  printf("Please enter a command : "); 
  int size = 0;     // initialize size to 0;
  char** params = getInput(&size);   // params now points to an array of strings based on the user input 
  while(!interpret(params, &image, size)) {  // display menu after each implementation when not hitting 'q' 
    displayMenu();
    printf("Please enter a command : ");
    for (int i = 0; i <size; i++) {  // free the memory for each string 
      free(params[i]);
    }
    
    free(params);       // free the memory for the array 
    params = getInput(&size);   // read in another command 
  }
  for (int i = 0; i< size; i++) {    //free memory for each string 
    free(params[i]);
  }
  free(params);    // free memory for the array 
  free(image.pixels);  // free memory for the pixels 

  return 0;
}


