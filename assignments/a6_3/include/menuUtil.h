/*menuUtil.c                                                                                                            
 *Cindy Yang                                                                                                      
 *Varun Radhakrishnan                                                                                             
 * <10/16/2016>                                                                                                   
 *headers of function needed in menuUtil.c                                                                                                    
                                                                                                                  
 */

#include <stdio.h>
#include "imageManip.h"

int displayMenu();
int interpret(char ** params, Image* image, int size);
char** getInput(int* n);
int handleInput();
