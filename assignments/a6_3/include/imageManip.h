/*imageManip.c                                                                                                            
 *Cindy Yang                                                                                                      
 *Varun Radhakrishnan                                                                                             
 * <10/16/2016>                                                                                                   
 * headers for funcitons needed for imageManip.c 
 */                                                                                                   
                                                                                       
#include "ppmIO.h"

int crop(Image* image, int x, int y, int x2, int y2);

int inversion(Image* image);

int swap(Image * image);
