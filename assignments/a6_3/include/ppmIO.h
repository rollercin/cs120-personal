/*ppmIO.h                                                                                                            
 *Cindy Yang                                                                                                      
 *Varun Radhakrishnan                                                                                             
 * <10/16/2016>                                                                                                   
 * headers of functions needed in ppmIO.h                                                                                                   
                                                                                                                  
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


#define MIN_BUFFER 4


typedef struct _pixel {

  unsigned char red;
  unsigned char green;
  unsigned char blue;
  
} Pixel;

typedef struct _image {

  Pixel* pixels;
  int row;
  int col;
  int colors;
  
} Image;

int readImage(char file[], Image* image);

int writeImage(char file[], Image* image);
char ignoreComments(FILE* fp);
int makeint(FILE* fp, char* number);
