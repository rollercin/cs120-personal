/*menuUtil.c                                                                                                      
 *Cindy Yang                                                                                                
 *Varun Radhakrishnan                                                                                       
 * <10/16/2016>                                                                                             
 * contain fucntions that prints out menu and read in user input                                                                                                                                                                            
 */
#include "../include/menuUtil.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

// print out the menu
int displayMenu() {
  printf("%s" , "Main menu:\n");
  printf("%s" , "\tr <filename> - read image from <filename>\n");
  printf("%s" , "\tw <filename> - write image to <filename>\n");
  printf("%s" , "\tc <x1> <y1> <x2> <y2> -  crop image to the box with the given corners\n");
  printf("%s" , "\ts - swap channels\n");
  printf("%s" , "\ti - invert intensities\n");
  printf("%s" , "\tl - convert to line art\n");
  printf("%s" , "\tg - convert to grayscale\n");
  printf("%s" , "\tbl <sigma> Gaussian blur with the given radius (sigma)\n");
  printf("%s" , "\tsh <sigma> <amt> - sharphen by given amount (intensity), with radius (sigma)\n");
  printf("%s" , "\tbr <amt> - change brightness (up or down) by the given amount\n");
  printf("%s" , "\tcn <amt> - change contrast (up or down) by the given amount\n");
  printf("%s" , "\tro - rotate the picture by 90 degree\n");
  printf("%s" , "\tq - quit\n");
  return 0;


}

//determine which function to call based on the characters in params
// params: a matrix of pointers that point to each component of input separated by white spaces
// image: image struct
// size : size of the params 
int interpret(char ** params, Image* image, int size) {
  if (size == 0 || strlen(params[0]) > 2) {   // no input   
    printf("Invalid Input\n");
    return 0;
  }
  switch(params[0][0]) {     

  case 'r' : {    // if the first character of the first string is r, print the second string which contains the address of
                  // the image file and call readImage()function  
    if (size == 2 && strlen(params[0]) == 1) {
      printf("Reading image at %s\n" , params[1]);
      readImage(params[1] , image);
    }else if(strlen(params[0]) == 2 && params[0][1] == 'o'){  //check if the input is ro"rotate the image"
      if((*image).pixels == NULL) {     // check if the image was read in or not   
	printf("Please read an image\n"); 
	return 0;
      }
      rotate(image);
      printf("rotating image by 90 degree\n");
    }
    else {         // invalid if there's more than two strings of input and the length of the first string('r') is not 1
      printf("Invalid Input: Please try again\n");
    }
    
  }
    break;

  case 'w' : {    // if the first character of the first string is w, print the second string which contains the address of
                  // the output file and call writeImage()function               
    if ((*image).pixels != NULL && size == 2 && strlen(params[0]) == 1)  {
      printf("Writing image at %s\n" , params[1]);
      writeImage(params[1], image);
    }
    else {  // invalid input if the pixels is not pointing to anything(haven't read anything in), or there are more then two
            //strings of input and the length of the first string('w') is not 1
      printf("Invalid Input\n");
    }
  }
    break;

  case 'c' : {   //if the first character of the first string is c     
    if (strlen(params[0]) == 2 && params[0][1] == 'n') {   // check if it's 'cn'
      double amount = atof(params[1]);                    //convert character into float 
      if (amount <= 0) {
	printf("Invalid input.  0 is not valid.\n");    //fault if contrast value is less than 0
	return 0;
      }

      if ((*image).pixels == NULL) {     //check if read in the image 
	printf("No image loaded\n");
	return 0;
      }
      contrast(image, amount);    // call contrast 
      printf("Contrasted Image\n");
      return 0;
    }
    if (strlen(params[0]) != 1) {   // check if there's more than one character in the first string 
      printf("Invalid input\n");
      return 0;
    }
    printf("Cropping image\n");   
    if (size != 5) {         // if there's more than five input strings, print 'invalid'
      printf("Invalid input\n");
      return 0;
    }
    if ((*image).pixels == NULL) {   //check if we have read in the image or not  
      printf("Please load an image\n");
      return 0;
    }
    int isValid = 1;  // converting strings of numbers to integer
                      // check if the string contain all numbers 
    for (int i = 1; i < size; i++) {
      if (!atoi(params[i])) {
	isValid = 0;     // sets isvalid to 0, if there's character other than num 
      }
    }

    if (isValid) {    // call crop if input is valid       
      crop(image, atoi(params[1]), atoi(params[2]), atoi(params[3]), atoi(params[4]));
    }
    else {
      printf("Invalid Input\n");
    }
  }
    break;

  case 'i' : {  //if the first character of the first string is i
                
   printf("Inverting image\n");
   if (strlen(params[0]) > 1) {    // check if the first string has more then one character 
     printf("Invalid input\n");
     return 0;
   }
   if ((*image).pixels != NULL && size == 1) {   // check if we have read in the image, and if there's more
                                                 //than one string of input
     inversion(image);                         
   }
   else {
     printf("Please read a file\n");
   }
  }
   break; 
  

   case 'l' : {  //if the first character of the first string is l
                
   printf("Converting to line art\n");
   if (strlen(params[0]) > 1) {    // check if the first string has more then one character 
     printf("Invalid input\n");
     return 0;
   }
   if ((*image).pixels != NULL && size == 1) {   // check if we have read in the image, and if there's more
                                                 //than one string of input
     lineArt(image);                         
   }
   else {
     printf("Please read a file\n");
   }
  }
   break;
   
  case 's' : {     //if the first character of the first string is s
    if (strlen(params[0]) == 2 && params[0][1] == 'h') {  // check if its 'sh'
      if (size != 3) {
	printf("Invalid Input\n");
	return 0;
      }

      if ((*image).pixels == NULL) {       
	printf("Please read a file\n");
	return 0;
      }

      double sigma = atof(params[1]);  // read in sigma      
      double amount = atof(params[2]); // read in amount(how many times sharper)
      if (sigma && amount) {
	sharpen(image, sigma, amount);  // call sharpen 
      }
      else {
	printf("Invalid Input\n");
        return 0;
      }
     return 0;
    }
    else if (strlen(params[0]) != 1) {  // check if the input string contains characters other than s 
      printf("Invalid input\n");
      return 0;
    }
   printf("Swapping channels\n");     
   if ((*image).pixels != NULL && size == 1) {  // check if we have read in the image, and is there's more
                                                //than one string of input
     swap(image);
   }
   else {
     printf("Please read an image\n");
   }
  }
    break;
    
  case 'g' : {    //if the first character of the first string is g
    if ((*image).pixels != NULL && size == 1 && strlen(params[0]) == 1)  {
      printf("Grayscaling image\n");
      grayscale(image);
    }
    else {  // invalid input if the pixels is not pointing to anything(haven't read anything in), or there are more then two
            //strings of input and the length of the first string('g') is not 1
      printf("Invalid Input or image not read\n");
    }
  }

    break;
  case 'b' : {     //if the first character of the first string is b
    if (strlen(params[0]) == 2 && params[0][1] == 'l') {  // if its blur 
      if (size != 2) {
	printf("Please specify an amount\n");
	return 0;
      }
      float sigma = atof(params[1]);
      if (sigma == 0) {
	printf("Please enter a valid value in.  0 is invalid\n");
	return 0;
      }
      if ((*image).pixels == NULL) {
	printf("Please read in an image\n");
	return 0;
      }
      blur(image, sigma);
    }
    else if(strlen(params[0]) == 2 && params[0][1] == 'r') {  // if its 
      if (size != 2) {
	printf("Please specify an amount\n");
	return 0;
      }
      int amount = atoi(params[1]);
      if (amount == 0) {
	printf("Please enter a valid value in.  0 is invalid\n");
	return 0;
      }
      if ((*image).pixels == NULL) {
	printf("Please read in an image\n");
	return 0;
      }
      brighten(image, amount);
    }
  }
    break;
  case 'q' : {    //if the first character of the first string is q
   printf("GOOD BYE\n");  
   return 1;   // quit the menu and end program  
  }
    break;
  default : {   
    printf("Invalid Input\n");  // case when there is no valid input 
  }
 }
 return 0;
}

// read user input into an array of strings  
char** getInput(int* n) {   //n is the size of the array
  int counter = 0;         // counter tracks the string length of each string 
  int wordCounter = 0;     //wordCouter tracks the number of strings 
  char c = NULL;         
  int capacity = 10;       // set the initial length of each string to 10 
  char** word = malloc(sizeof(char*) * 5);  // allocate memory of five character pointers to the array 
 
  for (int i = 0; i < 5; i++) {        // allocate memory for each string in the array 
    *(word + i) = malloc(sizeof(char) * capacity);
  }
  // keep reading in characters until '\n'
  while (c != '\n') {   

    if (wordCounter >= 5) {    // check if the size of the array is bigger than 5 
      printf("%s" , "Too many parameters: Try again\n");
      for(int i = 0; i < 5; i++) {
	free(*(word + i));   // free each string 
      } 
      free(word); // free the array
      while ((c = getchar()) != '\n'); //clears buffer
      return NULL;
    }

    	
    while(!isspace(c=getchar())) {    

      *(*(word + wordCounter) + counter) = c;  // put characters into word[wordCounter][counter]
      counter++;                               // let counter points to the next index in the string 

      if (counter >= capacity) {               //reallocate memory for the string if it exceeds the capacity     
	*(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * 2 * capacity);
	capacity = 2 * capacity;
      }
    }
      
    *(*(word + wordCounter) + counter) = '\0';  // add the null terminator to each string 
    if ( *(*(word + wordCounter)) == '\0' ) {
      wordCounter--;
    }
    else {
      *(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * (counter + 1)); //reallocate memory for the string make sure the size is exact
    }
    wordCounter++;  // read next string in the array 


    counter = 0;     //reset counter and capacity 
    capacity = 10;
  }

  //free the memory for each string 
  for (int i = wordCounter; i < 5; i++) {   
    free(*(word + i));
  }
  //free the memory for the array 
  word = realloc(word, (wordCounter) * sizeof(char*)); // reallocate memory for the array based on the
                                                       // number of strings we read in 
  *n = wordCounter;   // set the size of the array to the number of string we actually read in 
  return word;       // return the array 
}

//handles the user input and calls other helper functions 
int handleInput() {
  Image image;
  image.pixels = NULL;  // initialize pixels to NULL 
  displayMenu();   // prints out menu 
  printf("Please enter a command : "); 
  int size = 0;     // initialize size to 0;
  char** params = getInput(&size);   // params now points to an array of strings based on the user input 
  while(!interpret(params, &image, size)) {  // display menu after each implementation when not hitting 'q' 
    displayMenu();
    printf("Please enter a command : ");
    for (int i = 0; i <size; i++) {  // free the memory for each string 
      free(params[i]);
    }
    
    free(params);       // free the memory for the array
    size = 0;
    params = getInput(&size);   // read in another command 
  }
  for (int i = 0; i< size; i++) {    //free memory for each string 
    free(params[i]);
  }
  free(params);    // free memory for the array 
  free(image.pixels);  // free memory for the pixels 

  return 0;
}
