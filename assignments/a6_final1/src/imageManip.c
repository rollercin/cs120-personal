
/*imageManip.c                                                                                                      
 *Cindy Yang xyang73                                                                                                
 *Varun Radhakrishnan vradhak2                                                                                       
 * <10/16/2016>                                                                                             
 * functions needed for manipulation of images                                                                                             
 */

#include "../include/imageManip.h"
//first specify two corners upper left and lower right
//then crop the image based on the differences between the two
int crop(Image* image, int x, int y, int x2, int y2) {
  if (y2  <  y || y2 > (*image).row || y < 0) {    //checks if the coordinate provided are invalid 
    printf("Coordinates are wrong\n");
    return 1;
  }
  if (x2 < x || x2 > (*image).col || x < 0) {
    printf("Coordinates are wrong\n");
    return 1;
  }
  int row = y2 - y;      //new row       
  int col = x2 - x;      //new col 
  Pixel* newPixels = malloc(sizeof(Pixel) * row * col); //allocate memory for new image 
  for (int r = y; r<y2; r++) {                 // copy new pixel from the old pixel based on the new size
    for (int c = x; c < x2; c++) {             // of the image 
      newPixels[(r - y) * col + (c - x)] = (*image).pixels[r * (*image).col + c];
    }
  }
  free((*image).pixels);               //free memory allocated 
  (*image).pixels = newPixels;         // put updated pixel, row and col into image 
  (*image).row = row;
  (*image).col = col;
  return 0;
}
//invert each color value
int inversion(Image* image) {
  for (int r = 0; r<(*image).row; r++) {       // traverse all the pixels and invert each colors color value 
    for (int c = 0; c<(*image).col; c++) {
      (*image).pixels[r * (*image).col + c].red = (*image).colors - (*image).pixels[r * (*image).col + c].red;
      (*image).pixels[r * (*image).col + c].green = (*image).colors - (*image).pixels[r * (*image).col + c].green;
      (*image).pixels[r * (*image).col + c].blue = (*image).colors - (*image).pixels[r * (*image).col + c].blue;
    }
  }
  return 0;
}
//copy each color component's value to the next color component 
int swap(Image* image) {

  for (int r = 0; r<(*image).row; r++) {     // traverse all the pixels
     for (int c = 0; c<(*image).col; c++) {
      unsigned char temp = (*image).pixels[r * (*image).col + c].red; //put the color component of red in temp  
      (*image).pixels[r * (*image).col + c].red = (*image).pixels[r * (*image).col + c].green;  // green -- > red  
      (*image).pixels[r * (*image).col + c].green = (*image).pixels[r * (*image).col + c].blue; // blue -- > green 
      (*image).pixels[r * (*image).col + c].blue = temp;   // red -- > blue 
    }
  }

  return 0; 
    
}

int grayscale(Image* image) {

    for (int r = 0; r<(*image).row; r++) {     // traverse all the pixels
     for (int c = 0; c<(*image).col; c++) {
       unsigned char intensity = .30 * (*image).pixels[r * (*image).col + c].red + .59 * (*image).pixels[r * (*image).col + c].green + .11*(*image).pixels[r * (*image).col + c].blue; //finds the grayscale intensity for each pixel 
      (*image).pixels[r * (*image).col + c].red = intensity;  // sets each color to the intensity
      (*image).pixels[r * (*image).col + c].green = intensity;
      (*image).pixels[r * (*image).col + c].blue = intensity;
    }
  }

    return 0;
}
//traverse the image add the same value(amount) to each color in each pixel
int brighten(Image *image, int amount) {
  for (int r = 0; r < (*image).row; r++) {
    for  (int c = 0; c < (*image).col; c++) {
      brightenColor(&((*image).pixels[r * (*image).col + c].red), amount);   //call brightenColor, sent in the address of the color and amount to change
      brightenColor(&((*image).pixels[r * (*image).col + c].blue), amount);
      brightenColor(&((*image).pixels[r * (*image).col + c].green), amount);
    }
  }
  return 0;
}

//add amount to color value of each color value send in, and make sure its within the range 0-255 
int brightenColor(unsigned char* color, int amount) {
  signed int signInt = *color;
  signInt += amount;
  if (signInt > 255) {
    signInt = 255;
  }

  if (signInt < 0) {
    signInt = 0;
  }
  *color = signInt;
  return 1;
}
//increase the contrast of the imgae based on the ammount given 
int contrast(Image *image, double amount) {
  for (int r = 0; r < (*image).row; r++) {
    for  (int c = 0; c < (*image).col; c++) {

      double color = intToDouble((*image).pixels[r * (*image).col + c].red);   // call intToDouble function
      color = color * amount; //multiply the scaled color by amount
      (*image).pixels[r * (*image).col + c].red = doubleToInt(color); // call doubleToInt function

      color = intToDouble((*image).pixels[r * (*image).col + c].blue);
      color = color * amount;
      (*image).pixels[r * (*image).col + c].blue = doubleToInt(color);

      color = intToDouble((*image).pixels[r * (*image).col + c].green);
      color = color * amount;
      (*image).pixels[r * (*image).col + c].green = doubleToInt(color);
    }
  }

  return 0;
}
//convert color from the range 0-255 to the range -63.5-64
//return the converted color 
double intToDouble(unsigned char color) {
  double newColor;
  newColor = (color - 127)/(2.0);
  return newColor;
}
//convert color back to the range 0-255, meanwhile check if its out of the range
//return the color converted 
unsigned char doubleToInt(double color) {  
  int newColor = (color * 2.0) + 127;
  if (newColor < 0) {
    newColor = 0;
  }
  if (newColor > 255) {
    newColor = 255;
  }
  char RGBcolor = (char)newColor;
  return RGBcolor;
}
// blur the picture by adjusting each pixel value based on the pixel value surrongding it
int blur(Image *image, double sigma) {    // sigma determines the size of the matrix we are obtaining at the end 
  int size;   // size of the gaussianMatrix 
  double *matrix = gaussianMatrix(sigma, &size);//generate the gaussianMatrix based on sigma
  int ro = image->row;
  int co = image->col;
  Pixel* newpix = malloc(sizeof(Pixel) * ro * co); //allocate memory for a newpix
  for (int i = 0; i < ro; i++) {                 // copy new pixel from the old pixel 
    for (int j = 0; j < co; j++) {             // of the image
      newpix[i * co + j] = (*image).pixels[i*co +j];
    }
  }
  for (int r = 0; r < (*image).row; r++) { 
    for  (int c = 0; c < (*image).col; c++) {       // call get color change each pixel in image based on the sum of matrix * newpix
      getColor(image, matrix, r, c, size, newpix);
  }
 }
  free(matrix);  // free memory 
  free(newpix);
  return 0;
}
//generate the gaussianMatrix based on sigma
double* gaussianMatrix(double sigma, int* n) {
  *n = 10 * sigma;     
  if (*n % 2 == 0) {   //if sigma is even add one to it 
    *n += 1;
  }
  int size = *n;    
  double* matrix = malloc((*n) * (*n) * sizeof(double));  //allocate memory for the matrix  
  for (int dy = -size/2; dy <=  size/2; dy++) {     // traverse the matrix and calculate g value based on the formula
    for (int dx = -size/2; dx <= size/2; dx++) {
      matrix[(dy + size/2) * size + (dx + size/2)] = (1.0/(2.0 * PI * sigma * sigma)) * exp( -1 * (dx * dx + dy * dy) / (2 * sigma * sigma));
    }
  }

  return matrix;   
}

int getColor(Image * image, double* matrix,  int currentRow, int currentColumn, int size, Pixel* newpix)
{
  double red = 0;
  double blue = 0;
  double green = 0;
  double total = 0;
  // traverse the the matrix and the pixels aroung the current pixel with the same size of the matrix,
  //add the sum of the product of each of these color value  
  for (int dy = -size/2; dy <=  size/2; dy++) {
    for (int dx = -size/2; dx <= size/2; dx++) {
      if ((dy + currentRow >= 0) && (dy + currentRow < (*image).row) && (dx + currentColumn >= 0) && (dx + currentColumn < (*image).col)) {
	red += matrix[(dy + size/2) * size + (dx + size/2)] *  newpix[(dy + currentRow) * (*image).col + (dx + currentColumn)].red;
	blue += matrix[(dy + size/2) * size + (dx + size/2)] * newpix[(dy + currentRow) * (*image).col + (dx + currentColumn)].blue;
	green += matrix[(dy + size/2) * size + (dx + size/2)] * newpix[(dy + currentRow) * (*image).col + (dx + currentColumn)].green;
	total += matrix[(dy + size/2) * size + (dx + size/2)];
      }
    }
  }
  //write the newpix into to pixels
  //prevent the change in pixels value affectd the next pixel called
  //since we are getting pixels valude from newpix which is a copy of pixels
  image->pixels[(currentRow) * (*image).col + (currentColumn)].red = (unsigned char)(red/total);
  image->pixels[(currentRow) * (*image).col + (currentColumn)].green = (unsigned char)(green/total);
  image->pixels[(currentRow) * (*image).col + (currentColumn)].blue = (unsigned char)(blue/total);

  return 0;
}
//reverse the effect of blurring sharpen the image
int sharpen(Image* image, double sigma, double amount) {
  Image* blurredPic = malloc(sizeof(Image));  //create a copy of the original image  
  (*blurredPic).row = (*image).row;
  (*blurredPic).col = (*image).col;
  Pixel* blurredPixels = malloc(sizeof(Pixel) * (*image).row * (*image).col);
  for (int r = 0; r <(*image).row; r++) {
    for (int c = 0; c < (*image).col; c++) {
      blurredPixels[r * (*image).col + c] = (*image).pixels[r * (*image).col + c];
    }
  }
  (*blurredPic).pixels = blurredPixels;
  blur(blurredPic, sigma);   //blur that copy of image 

  for (int r = 0; r <(*image).row; r++) {       // traverse each pixel in the original image, and change each of its color value 
                                                
    for (int c = 0; c < (*image).col; c++) {     
      (*image).pixels[r * (*image).col + c].red = calculateDifference((*image).pixels[r * (*image).col + c].red, blurredPixels[r * (*image).col + c].red, amount);
      (*image).pixels[r * (*image).col + c].green = calculateDifference((*image).pixels[r * (*image).col + c].green, blurredPixels[r * (*image).col + c].green, amount);
      (*image).pixels[r * (*image).col + c].blue = calculateDifference((*image).pixels[r * (*image).col + c].blue, blurredPixels[r * (*image).col + c].blue, amount);
    }
  }

  free(blurredPixels);
  free(blurredPic);
  return 0;
  
}
//change the color value of the original pix to sharpen the image by
//first calculate teh difference between the initial color value and the blurred color value
//mutiply that by amount(sharpen intensity)
//keep the range inside 0-255
unsigned char calculateDifference(unsigned char color, unsigned char blurredColor, double amount) {
  double difference = color - blurredColor;
  difference = difference * amount;
  signed int newColor = color + difference;
  if (newColor < 0) {
    newColor = 0;
  }
  if (newColor > 255) {
    newColor = 255;
  }
  return (unsigned char)(newColor);
}

//rotate the image by 90 degree 
int rotate(Image* img){
  int r = img->row;      
  int c = img->col;
  Pixel* newpix = malloc(sizeof(Pixel) * r * c);  //allocate memory for the new pixel
  for(int i = 0; i < c; i++){      //traverse pixels and put each pixel into a different spot in the newpix(rotate 90)  
    for(int j = 0; j < r; j++){
      newpix[i*r + j] = img->pixels[(r-1-j)*c + i];  
    }
  }
  img->row = c;   //change row and col of the new image 
  img->col = r;
  free(img->pixels);
  img->pixels = newpix;   
  return 0;
}

int lineArt(Image* image) {
  Pixel* copy = malloc(sizeof(Pixel) * (*image).row * (*image).col);    //copy the original image 
  for (int r = 0; r < (*image).row; r++) {
    for (int c = 0; c < (*image).col; c++) {
      copy[r * (*image).col + c] = (*image).pixels[r * (*image).col + c];
    }
  }

  for (int r = 0; r <(*image).row; r++) {      //traverse all the pixel and set each of its color value to
                                               //either 255 or 0 
    for (int c = 0; c < (*image).col; c++) {
      getShade(image, copy, r, c);
    }
    
  }
  free(copy);
  return 0;
}
//set the color value to either black or white 
int getShade(Image* image, Pixel* copy,  int currentRow, int currentCol){
  for (int dy = -1; dy <= 1; dy++) {       // traverse the up down left and right directions and find the         
    for (int dx = -1; dx <= 1; dx++) {    // intensity value of its pixel by calling getGreyScaleValue
                                          // if the pixel is not out of bound   
      if ( (currentRow + dy > 0) && (currentRow + dy <= (*image).row) && (currentCol + dx > 0) && (currentCol + dx < (*image).col) ) {
	int adjacentPixelvalue = getGrayscaleValue(copy[(dy + currentRow) * (*image).col + (dx + currentCol)]);
	int currentPixelvalue = getGrayscaleValue(copy[currentRow * (*image).col + currentCol]);
	int difference = adjacentPixelvalue - currentPixelvalue;
	if (abs(difference) > 20) {        // set the pixel to black as long as there is one pixel around it has a intensity difference  
	                                   //bigger than 20 which means its a margin 
	                                   
	  (*image).pixels[currentRow * (*image).col + currentCol].red = 0;
	  (*image).pixels[currentRow * (*image).col + currentCol].green = 0;
	  (*image).pixels[currentRow * (*image).col + currentCol].blue = 0;
	  return 1;
	}
      }
    }
  }
  //otherwise set the pixel to white 
  (*image).pixels[currentRow * (*image).col + currentCol].red = 255;
  (*image).pixels[currentRow * (*image).col + currentCol].green = 255;
  (*image).pixels[currentRow * (*image).col + currentCol].blue = 255;

  return 0;
}
//calculate the intensity value of each pixel 
int getGrayscaleValue(Pixel p) {  
  return .30 * p.red + .59 * p.green + .11* p.blue;
} 
