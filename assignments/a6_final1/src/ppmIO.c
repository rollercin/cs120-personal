
/*ppmIO.c		  

 *Cindy Yang xyang73
 *Varun Radhakrishnan vradhak2
 * <10/16/2016>
 * contains fucntions to read in the image file and write the new image file while also ignoring the comments in the image
 * file                                      
 */
#include "ppmIO.h"
//read in all the characters in a single comment 
char ignoreComments(FILE* fp) {

  char c;
  while ((c = fgetc(fp)) == '#') {
    while((c = fgetc(fp)) != '\n');  //keep reading input character until it reaches '\n'
  }
  return c;    // return the first character of the type 
}
// parses a string into an integer 
int makeInt(FILE* fp, char *number) {
   int counter = 1; 
   char c;
   while (!isspace((c = fgetc(fp)))) {
     if (isdigit(c)) {    // checks if the input is number 
       number[counter] = c;  
    }
    else {
      printf("Invalid file\n");
      return 0;
    }
    counter++;
    if (counter > 10) {    // checks if the number of digits are greater than 10 (max int has 10 digits) 
      printf("File is too large\n");
      return 0;
    }
  }
   number[counter] = '\0';   // add the null terminator to the number string 
   int num = atoi(number);    // convert the num into integer 
  return num;  
}
//read the pixels, row, col and type from the input image file, and put them into the image struct  
int readImage(char file[], Image* image) {
  FILE* fp = fopen(file, "rb");   // open the image file to read from 
  free((*image).pixels);          // free the memory of the previous image  
  if (fp == NULL) {              // check is the file is empty or not 
    printf("File reading error\n");
    return 1;
  }
  char type[2];  // string of the type of the image   
  int row;       
  int col;
  int colors;    
  type[0] = ignoreComments(fp); // type[0] is the first character of the type if there's comment before type  
  if (type[0] == EOF) {         // check if it contains valid information in the file 
    printf("File reading error\n");
    fclose(fp);
    return 1;
  }
  
  for (int x = 1; x<2; x++) {   // keep reading the characters into type 
    type[x] = fgetc(fp);
    if (type[x] == EOF) {       // checks if it contains valid information in the file
      printf("File reading error\n");
      fclose(fp);
      return 1;
    }
  }
  // chech if the type is P6 and if there's other characters after P6 
  if (fgetc(fp) != '\n' || type[0] != 'P' || type[1] != '6') {
    printf("File reading error\n");
    fclose(fp);
    return 1;
  }
                                              //allocate memory for column
  char *column = malloc(sizeof(char) * 11);
  column[0] = ignoreComments(fp);  // ingore comment if there is any and return the first character of the
                                   // column string 
  col = makeInt(fp, column);       // convert string into integer 
  free(column);
                                                //allocate memory for r
  char *r = malloc(sizeof(char) * 11);          //ignore comment if there is any and return the first character  
  r[0] = ignoreComments(fp);                   // of the r string 
  row = makeInt(fp , r);                      // convert string into integer 
  free(r);
                                                //allocate memory for color
  char *color = malloc(sizeof(char) * 11);
  color[0] = ignoreComments(fp);               // same step as column and s   
  colors = makeInt(fp, color);
  free(color);
  if (col == 0 || row == 0 || colors == 0) {  // invalid input 
    fclose(fp);
    return 1;
  }
  Pixel* pixels = malloc(row * col * sizeof(Pixel));  // allocate memory for pixel 
  fread(pixels, sizeof(Pixel), row * col, fp);      // read pixel array, sizeof(Pixel), row and col from the file
  (*image).pixels = pixels;              //  create the image struct
  (*image).row = row;
  (*image).col = col;
  (*image).colors = colors;
  fclose(fp);
  return 0;
}
// write the image into the output ppm file 
int writeImage(char file[], Image* image) {

  FILE* fp = fopen(file, "wb");
  fprintf(fp, "%s\n%d %d\n%d\n", "P6", (*image).col, (*image).row, (*image).colors); //print info at the begining                                                                            // of the file  
  fwrite((*image).pixels, sizeof(Pixel), (*image).row * (*image).col, fp); // write the pixels 
  fclose(fp);  // close the file 
  return 0;
}

