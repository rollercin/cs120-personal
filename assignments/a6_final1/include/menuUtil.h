/*menuUtil.h                                                                                                            
 *Cindy Yang                                                                                                      
 *Varun Radhakrishnan                                                                                             
 * <10/16/2016>                                                                                                   
 *headers of function needed in menuUtil.c                                                                                                    
                                                                                                                  
 */

#include <stdio.h>
#include "imageManip.h"

int displayMenu(); //prints the menu
int interpret(char ** params, Image* image, int size); //interprets the user input
char** getInput(int* n); //converts the user input into an array of strings
int handleInput(); //acts as the bridge to other functions (main handler of user input)
