/*ppmIO.h                                                                                                            
 *Cindy Yang xyang73                                                                                                     
 *Varun Radhakrishnan vradhak2                                                                                             
 * <10/16/2016>                                                                                                   
 * headers of functions needed in ppmIO.h                                                                                                   
                                                                                                                  
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


#define MIN_BUFFER 4


typedef struct _pixel { //contains the color information of a pixel

  unsigned char red;
  unsigned char green;
  unsigned char blue;
  
} Pixel;

typedef struct _image { //contains information about an image

  Pixel* pixels; //array of pixels in image
  int row; //size of row and col
  int col;
  int colors; //amount of colors in picture
  
} Image;

int readImage(char file[], Image* image); // reads the image file into an image struct

int writeImage(char file[], Image* image); //writes an image to a file
char ignoreComments(FILE* fp); //helper function to ignore comments
int makeint(FILE* fp, char* number); //converts a string into an integer (helper function for readImage)
