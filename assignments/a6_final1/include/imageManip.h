/*imageManip.h                                                                                                            
 *Cindy Yang xyang73                                                                                                    
 *Varun Radhakrishnan vradhak2                                                                                             
 * <10/16/2016>                                                                                                   
 * headers for funcitons needed for imageManip.c 
 */                                                                                                   
                                                                                       
#include "ppmIO.h"
#include <math.h>
#define PI 3.14159625
int crop(Image* image, int x, int y, int x2, int y2); //crops the image from the top left corner to the bottom right corner

int inversion(Image* image); //inverts the color channels of the image

int swap(Image * image); //swaps the color channels of the image

int grayscale(Image *image); //converts the image to grayscale

int brighten(Image *image, int amount);

int brightenColor(unsigned char* color, int amount);

int contrast(Image* image, double amount);

double intToDouble(unsigned char color);  // helper function for contrast 

unsigned char doubleToInt(double color);  // helper function for contrast 

int blur(Image* image, double sigma);

double* gaussianMatrix(double sigma, int* size);

int getColor(Image* image, double * matrix, int currentRow, int currentCol, int size, Pixel* newpix);

int sharpen(Image* image, double sigma, double amount);

unsigned char calculateDifference(unsigned char color, unsigned char otherColor, double amount);


int lineArt(Image * image);   //new feature 

int getShade(Image* image, Pixel* copy, int currentRow, int currentCol);

int getGrayscaleValue(Pixel p);   

int rotate(Image* img); //new feature 
