/*purpose: get characters and print the number of characters out
 *author:Cindy Yang, Stanley Wang 
 *file created on: 09/16/2016
 *latest update: 09/16/2016
 */
#include<stdio.h>

int main(){

  //define some variables
  int count = 0;
  char a;
  a = getchar();

  //count the number of input characters
  while(a !=EOF ){
    count++ ;
    a = getchar();
  }

  //print out the number of characters
  printf("-------------\n");
  printf("%d characters\n",count);

  return 0;
}
