/*purpose: get characters and print the number of characters, words and lines.
 *author:Cindy Yang, Stanley Wang 
 *file created on: 09/16/2016
 *latest update: 09/16/2016
 */
#include<stdio.h>
#include<ctype.h>
int main(){

  //define variables 
  int chars = 0;
  int words = 0;
  int lines = 0;
  char a;
  int bool = 0;     //bool is false when outside a word, true when inside

  //count the number of lines, words and characters
  while((a = getchar()) && (a != EOF) ){
    
    if (a == '\n'){     //count lines 
      lines ++;
    }

    if (isspace(a) || a == '\n') {
      bool = 0;               //set bool to false when meet a space a new line
    } else if (bool == 0) {
      if (!ispunct(a)) {      //set bool to true and count word only when meet
	words ++;             //a new letter
	bool = 1;
      }
    }
    chars++ ;
  }
  
  //print out the number of lines,number of words,and the number of characters
  printf("-------------\n");
  printf("%d lines\n", lines);
  printf("%d words\n", words);
  printf("%d characters\n", chars);
  
  return 0;
}
