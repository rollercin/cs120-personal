/*purpose: gets character and prints them back to the terminal
 *author:Cindy Yang, Stanley Wang 
 *file created on: 09/16/2016
 *latest update: 09/16/2016
 */
#include<stdio.h>
int main(){
  
  //define variable a
  char a;
  a = getchar();

  //print out the input characters
  while(a !=EOF ){
    printf("%c",a);
    a = getchar();
  }
  
  return 0;
}
