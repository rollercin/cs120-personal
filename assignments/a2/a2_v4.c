/*purpose: get characters and print the number of lines, words, characters and *calculate the average
 *length of the word. 
 *author:Cindy Yang, Stanley Wang 
 *file created on: 09/16/2016
 *latest update: 09/17/2016
 */
#include<stdio.h>
#include<ctype.h>

int main(){

  //define variables
  int chars = 0;
  int words = 0;
  int lines = 0;
  char a;
  int bool = 0;      //set to false when outside the word, true when inside
  float wordChars = 0;
  float avgWordLength = 0;
  
  //count the number of chars, words, lines, word-characters
  while((a = getchar()) && (a != EOF) ){
    if (a == '\n'){         //count lines
      lines ++;
    }

    if (isspace(a) || a == '\n') { //set bool to false when meet space or "\n"
      bool = 0; 

    } else if (bool == 0) {

      if (!ispunct(a)) { //set bool to true and count words when meet a letter
	words ++;
	wordChars ++;    //count the first word-character
	bool = 1;
       }

    } else if (bool == 1 && !ispunct(a)){
      wordChars ++;      //count word-characters inside a word
    }
    chars++ ;
  }

  //calculate average word length and print number of lines,words,characters
  //and words(if there is a least a word in the input file)
  avgWordLength = wordChars/words;
  printf("-------------\n");
  printf("%d lines\n", lines);
  printf("%d words\n", words);
  printf("%d characters\n", chars);
  if(wordChars != 0){
    printf("Avg word length: %3.1f characters\n", avgWordLength);
  } else {
    printf("Avg word length: 0.0 characters\n");
  }
  
  return 0;
}
