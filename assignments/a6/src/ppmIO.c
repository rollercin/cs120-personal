#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "ppmIO.h"
int readPPM(char file[], Image* img){
  int row, col, color;
  char type[2];
  FILE* fp = fopen(file,"rb");
  fscanf(fp,"%s",type);
  fscanf(fp,"%d",&row);  
  fscanf(fp,"%d",&col);
  fscanf(fp,"%d",&color);
  fgetc(fp);
  Pixel *pix = malloc((sizeof(Pixel)) * row * col);
  fread(pix, sizeof(Pixel), row*col, fp);
  img->data = pix;
  img->rows = row;
  img->cols = col;
  fclose(fp);
  return 0;
}

int writePPM(char file[], Image* img){
  FILE* fp = fopen(file, "wb");
  fprintf(fp,"%s\n%d %d\n%d\n","P6",img->rows,img->cols,255);
  fwrite(img->data,sizeof(Pixel),(*img).rows * (*img).cols, fp);
  return 0;
}


