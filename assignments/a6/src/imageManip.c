#include <stdlib.h>
#include <stdio.h>
#include "imageManip.h"
int crop(Image* img, int x, int y, int x1, int y1){
  int r = y1-y;
  int c = x1-x;
  img->rows = r;
  img->cols = c;
  Pixel* newpix = malloc(sizeof(Pixel)*r*c);
  for(int i = 0; i < r; i++){
    for(int j =0; j < c;j++){
      newpix(i*c + j) = img->data((y+i)*c + (x+j));
    }
  }
  img->data = newpix;
  free(newpix);
		     
  return 0;
}


int inversion(Image* img){
  int r = img->rows;
  int c = img->cols;
  Pixel* np = malloc(sizeof(Pixel)*r*c);
  for(int i = 0; i < r; i++){
    for(int j = 0; j < c; j++){
      np(i*r + j).r = 255 - (img->data(i*r + j).r);
      np(i*r + j).g = 255 - (img->data(i*r + j).g);
      np(i*r + j).b = 255 - (img->data(i*r + j).b);
    }
  }
  img->data = np;
  free(newpix);
  return 0;
}
