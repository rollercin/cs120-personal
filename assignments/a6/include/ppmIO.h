#ifndef _BEN120_MENUUTIL_H
#define _BEN120_MENUUTIL_H

#include <stdio.h>
typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
}Pixel;

typedef struct _image {
  Pixel* data;
  int rows;
  int cols;
  int colors;
}Image;

int readPPM(char file[], Image* img);

int writePPM(char file[], Image* img);

#endif
