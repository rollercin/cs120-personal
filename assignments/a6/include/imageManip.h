#ifndef _BEN120_IMAGEMANIP_H
#define _BEN120_IMAGEMANIP_H
//#include<stdio.h>

#include "ppmIO.h"
int crop(Image* img, int x, int y, int x1, int y1);

int inversion(Image* img);

int swap(Image* img);
#endif
