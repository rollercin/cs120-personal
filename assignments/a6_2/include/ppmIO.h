#include <stdio.h>
#include <stdlib.h>

#define MIN_BUFFER 4


typedef struct _pixel {

  unsigned char red;
  unsigned char green;
  unsigned char blue;
  
} Pixel;

typedef struct _image {

  Pixel* pixels;
  int row;
  int col;
  int colors;
  
} Image;

int readImage(char file[], Image* image);

int writeImage(char file[], Image* image);
