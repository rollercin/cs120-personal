#include "../include/menuUtil.h"
#include <ctype.h>
#include <stdlib.h>


int displayMenu() {
  printf("%s" , "Main menu:\n");
  printf("%s" , "\tr <filename> - read image from <filename>\n");
  printf("%s" , "\tw <filename> - write image to <filename>\n");
  printf("%s" , "\tc <x1> <y1> <x2> <y2> -  crop image to the box with the given corners\n");
  printf("%s" , "\ti - invert intensitities\n");
  printf("%s" , "\tg - convert to grayscale\n");
  printf("%s" , "\tbl <sigma> Gaussian blur with the given radius (sigma)\n");
  printf("%s" , "\tsh <sigma> <amt> - sharphen by given amount (intensity), with radius (sigma)\n");
  printf("%s" , "\tbr <amt> - change brightness (up or down) by the given amount\n");
  printf("%s" , "\tcn <amt> - change contrast (up or down) by the given amount\n");
  printf("%s" , "\tq - quit\n");
  return 0;


}


int interpret(char ** params, Image* image) {
 switch(params[0][0]) {

  case 'r' : {
    printf("Reading image at %s\n" , params[1]);
    readImage(params[1] , image);
  }
    break;

  case 'w' : {
    printf("Writing image at %s\n" , params[1]);
    writeImage(params[1], image);
  }
    break;

  case 'c' : {
    printf("Cropping image\n");
    crop(image, atoi(params[1]), atoi(params[2]), atoi(params[3]), atoi(params[4]));
  }
    break;

  case 'i' : {
   printf("Inverting image\n");
   inversion(image);
  }
   break;

  case 's' : {
   printf("Swapping channels\n");
   swap(image);
  }

    break;
 case 'q' : {
   printf("GOOD BYE\n");
   return 1;
 }
   
 }
 return 0;
}


char** getInput() {
  int counter = 0;
  int wordCounter = 0;
  char c = NULL;
  int capacity = 10;
  char** word = malloc(sizeof(char*) * 5);

  for (int i = 0; i < 5; i++) {
    *(word + i) = malloc(sizeof(char) * capacity);
  }
  
  while (c != '\n') {

    if (wordCounter >= 5) {
      printf("%s" , "Too many parameters: Try again\n");
      return NULL;
    }
	
    while(!isspace(c=getchar())) {

      *(*(word + wordCounter) + counter) = c;
      counter++;

      if (counter >= capacity) {
	*(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * 2 * capacity);
	capacity = 2 * capacity;
      }
    }

    *(word + wordCounter) = realloc(*(word + wordCounter), sizeof(char) * (counter + 1));
    *(*(word + wordCounter) + counter) = '\0';
    
    wordCounter++;

    
    counter = 0;
    capacity = 10;
  }
  word = realloc(word, (wordCounter) * sizeof(char*));

  return word;
}


int handleInput() {
  Image image;
  displayMenu();
  char** params = getInput();
  while(!interpret(params, &image)) {
    displayMenu();
    for (int i = 0; i <5; i++) {
      free(params[i]);
    }
    
    free(params);
    params = getInput();
  }
  
  return 0;
}
