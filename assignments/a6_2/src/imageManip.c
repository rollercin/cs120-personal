#include "../include/imageManip.h"

int crop(Image* image, int x, int y, int x2, int y2) {
  int row = y2 -y;
  int col = x2 - x;
  Pixel* newPixels = malloc(sizeof(Pixel) * row * col);
  for (int r = y; r<y2; r++) {
    for (int c = x; c < x2; c++) {
      newPixels[(r - y) * col + (c - x)] = (*image).pixels[r * (*image).col + c];
    }
  }
  free((*image).pixels);
  (*image).pixels = newPixels;
  (*image).row = row;
  (*image).col = col;
  return 0;
}

int inversion(Image* image) {
  for (int r = 0; r<(*image).row; r++) {
    for (int c = 0; c<(*image).col; c++) {
      (*image).pixels[r * (*image).col + c].red = (*image).colors - (*image).pixels[r * (*image).col + c].red;
      (*image).pixels[r * (*image).col + c].green = (*image).colors - (*image).pixels[r * (*image).col + c].green;
      (*image).pixels[r * (*image).col + c].blue = (*image).colors - (*image).pixels[r * (*image).col + c].blue;
    }
  }
  return 0;
}

int swap(Image* image) {

  for (int r = 0; r<(*image).row; r++) {
    for (int c = 0; c<(*image).col; c++) {
      unsigned char temp = (*image).pixels[r * (*image).col + c].red; 
      (*image).pixels[r * (*image).col + c].red = (*image).pixels[r * (*image).col + c].green;
      (*image).pixels[r * (*image).col + c].green = (*image).pixels[r * (*image).col + c].blue;
      (*image).pixels[r * (*image).col + c].blue = temp;
    }
  }

  return 0; 
    
}
